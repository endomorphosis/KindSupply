*********************************************************************

        D R U P A L   E C O M M E R C E   F R A M E W O R K

**********************************************************************

DESCRIPTION:

Drupal eCommerce 6.x-4 aims to provide an eCommerce framework for your Drupal
website.

Please check the release notes for full details of each release.


********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal 6.x up and running. Please back up
your site and database.

1. Download and install the token module:
   http://drupal.org/project/token
   
2. Download and install the rules module:
   http://drupal.org/project/rules

2. Download and install the views module:
   http://drupal.org/project/views
   
3. Download and place the entire ecommerce directory into your desired contrib
   modules location: eg. /sites/all/modules/

4. Enable core modules by going to:
   > Administer > Site Building > Modules.

   With E-Commerce 6.x-4.x, modules are grouped according to function.
   It is important to install modules in a particular order because of dependencies.

   Here is the correct order:

   (A) enable the Common and anonymous purchases modules
   (B) enable the customer.module
   (C) enable the receipting and store.modules
   (D) enable the Product API and the address.module
   (E) enable the checkout.module
   (F) enable the buy now, cart and availability.modules

   (Optional) The payment gateway module for paypal is included in the default
   download of Drupal eCommerce.

   Once you have enabled the main Drupal eCommerce modules you can now start to
   setup your shop by visiting ADMINISTER -> ECOMMERCE CONFIGURATION.

5. Drupal eCommerce 4 introduces a cleaner administration section. You can find E-Commerce
   settings in two blocks - Just click on Administer then click on:

   E-Commerce -- day to day administration of your store, managing orders etc.
   E-Commerce configuration -- your store settings.

   Individual module settings are also listed there with their descriptions.

6. Create new products via:
   > Create content > Product

7. Optionally, enable the cart block via
   > Administer > blocks :: Shopping Cart


********************************************************************
NOTES & LINKS

Current Maintainer  : Gordon Heydon http://drupal.org/user/959
Original Author     : Matt Westgate (not a current maintainer)

General Links:

Main Site           : http://www.drupalecommerce.com
Features list       : http://www.drupalecommerce.org/features
User Documentation  : http://www.drupalecommerce.org/documentation
Support             : http://www.drupalecommerce.org/help


Drupal.org Links:

Project Page         : http://drupal.org/project/ecommerce
Handbook             : http://drupal.org/node/50350
Support Queue        : http://drupal.org/project/issues/ecommerce
eCommerce user group : http://groups.drupal.org/ecommerce
Mailing List         : http://lists.heydon.com.au/listinfo.cgi/ecommerce-heydon.com.au
                      (Mailing list subject to change)

Developer Links:

Developers API       : http://www.drupalecommerce.org/api
Developers Docs      : http://www.drupalecommerce.org/developers
                       (Handbook pages are in development)
IRC                  : irc.freenode.org > #drupal-ecommerce
                     

