<?php
// $Id: ec_common_views_handler_field_format_currency.inc,v 1.1.2.2 2009/02/10 14:01:23 recidive Exp $

/**
 * @file
 * Implement handler to format currency.
 */

class ec_common_views_handler_field_format_currency extends views_handler_field {
  function render($values) {
    return format_currency($values->{$this->field_alias});
  }
}
