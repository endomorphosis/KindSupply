<?php
// $Id: ec_checkout.admin.inc,v 1.3.2.3 2009/02/10 14:01:22 recidive Exp $

/**
 * @file
 * eC Checkout administration.
 */

/**
 * Form for determining the order of the checkout screens.
 *
 * @ingroup form
 */
function ec_checkout_admin_screen_form() {
  $form = array();

  $types = ec_checkout_get_types();

  $form['ec_checkout_weights'] = array(
    '#tree' => TRUE,
    '#theme' => 'ec_checkout_admin_screen_form',
  );

  foreach ($types as $type => $info) {
    $form['ec_checkout_weights'][$type] = array();
    $form['ec_checkout_weights'][$type]['name'] = array(
      '#value' => $info->name,
    );
    $form['ec_checkout_weights'][$type]['description'] = array(
      '#value' => $info->description,
    );
    $form['ec_checkout_weights'][$type]['weight'] = array(
      '#type' => 'weight',
      '#default_value' => $info->weight,
      '#delta' => count($types) > 10 ? count($types)+1 : 10,
      '#attributes' => array('class' => 'screen-weight'),
    );
  }

  return system_settings_form($form);
}
