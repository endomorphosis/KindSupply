// $Id: ec_checkout.js,v 1.1.2.1 2009/03/14 11:25:02 gordon Exp $

Drupal.behaviors.ecCheckout = function () {
  $('#ec-checkout-form input, #ec-checkout-form select').change(
    function () {
      $('#edit-order').attr('disabled', 'disabled');
    }
  );
}

