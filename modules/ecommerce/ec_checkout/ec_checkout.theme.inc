<?php
// $Id: ec_checkout.theme.inc,v 1.2.2.4 2009/03/22 06:37:29 gordon Exp $

/**
 * @file
 * Handle all checkout theme items.
 */

function theme_ec_checkout_checkout_review_form(&$form) {
  $header = array(t('Item'), t('Qty'), t('Price'), t('Subtotal'), '');
  $rows = array();
  if (!empty($form['items'])) {
    foreach ($form['items'] as $key => $line) {
      if (is_numeric($key)) {
        $rows[] = array(
          $line['item']['#value']->title,
          $line['qty']['#value'],
          array('data' => format_currency($line['price']['#value']), 'align' => 'right'),
          array('data' => format_currency($line['subtotal']['#value']), 'align' => 'right'),
          $line['options']['#value']
        );
      }
    }
  }

  $rows[] = array('', '', '', '', '');
  foreach ($form['totals'] as $id => $line) {
    if (is_numeric($id)) {
      $rows[] = array(
        "<b>{$line['#title']}</b>",
        '',
        '',
        array('data' => isset($line['#value']) ? format_currency($line['#value']) : '', 'align' => 'right'),
        ''
      );
    }
  }

  $content = theme('table', $header, $rows);

  return theme('box', t('Order Summary'), $content);
}

/**
 * Themes the admin screen.
 * @ingroup themeable
 */
function theme_ec_checkout_admin_screen_form($form) {
  drupal_add_tabledrag('screen-table', 'order', 'sibling', 'screen-weight', 'screen-weight');
  $output = '';
  $header = array(t('Type'), t('Description'), t('Weight'));

  foreach (element_children($form) as $type) {
    $rows[] = array(
      'data' => array(
        drupal_render($form[$type]['name']),
        drupal_render($form[$type]['description']),
        drupal_render($form[$type]['weight']),
      ),
      'class' => 'draggable',
    );
  }

  $output .= theme('table', $header, $rows, array('id' => 'screen-table'));
  $output .= drupal_render($form);

  return $output;
}
