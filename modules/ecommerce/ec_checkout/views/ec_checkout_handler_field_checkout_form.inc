<?php
// $Id: ec_checkout_handler_field_checkout_form.inc,v 1.1.2.3 2009/03/04 12:09:48 gordon Exp $
/**
 * @file
 * Implements display handler for product types
 */

class ec_checkout_handler_field_checkout_form extends views_handler_field {
  function render($values) {
    $node = node_load($values->{$this->field_alias});

    if (isset($node->ptype) && ec_product_can_purchase($node)) {
      return drupal_get_form('ec_checkout_product_form', $node);
    }
  }
}
