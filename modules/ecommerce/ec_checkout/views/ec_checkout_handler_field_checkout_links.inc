<?php
// $Id: ec_checkout_handler_field_checkout_links.inc,v 1.1.2.3 2009/03/04 12:09:48 gordon Exp $
/**
 * @file
 * Implements display handler for product types
 */

class ec_checkout_handler_field_checkout_links extends views_handler_field {
  function render($values) {
    $node = node_load($values->{$this->field_alias});

    if ($links = module_invoke_all('link', 'checkout', $node)) {
      drupal_alter('link', $links, $node);
    }

    return theme('links', $links);
  }
}
