// $Id: ec_cart.js,v 1.1.2.1 2009/01/08 04:08:14 gordon Exp $

Drupal.ec_cart_load = function (cart) {
  $('#block-ec_cart-0')
    .each(
      function () {
        $(this).before(cart.full_cart)
        $(this).remove();
      }
    );
}

Drupal.ec_cart_add = function (href, callback, element) {
  $.getJSON(href, 
    function (json) {
      if (json.errors == undefined) {
        if (json.full_cart) {
          Drupal.ec_cart_load(json);
        }
        else {
          var item = $('#block-ec_cart-0 #'+json.block_id);
          var block_id = json.block_id;
          var block_item = json.block_item;
          var total = json.total;
          var items = json.items;

          if (item.size()) {
            $(item)
              .fadeOut(400,
                function () {
                  $(this).html(block_item);
                  $(this).fadeIn(400);
                }
              );
          }
          else {
            $('#block-ec_cart-0 .items')
              .append('<div id="'+ block_id +'" class="cart-item">'+ block_item +'</div>')
              .find('#block-ec_cart-0 .cart-item:last')
              .fadeIn(400);
          }
          $('#block-ec_cart-0 .total')
            .fadeOut(400, 
              function () {
                $(this)
                  .html(total)
                  .fadeIn(400);
              }
            );
          $('#block-ec_cart-0 .item-count em')
            .each(
              function () {
                $(this)
                  .html(items+' items');
              }
            );
          $('#block-ec_cart-0 .checkout:hidden')
            .each(
              function () {
                $(this)
                  .fadeIn(400);
              }
            );
        }
        if (callback != undefined) {
          callback.apply(element);
        }
      }
      else {
        alert(json.errors.join('\n'));
      }
    }
  );
  return false;
}

Drupal.ec_cart_convert_link = function () {
  var e = this;
  var href = Drupal.settings.ec_cart.add_path+'/'+$(this).attr('id').replace(/cart-item-/i, '');
  var q = $(this).attr('href').split('?');
  if (q[1] != undefined) {
    href+= '?'+q[1];
  }
  Drupal.ec_cart_add(href);
  return false;
}

$(document).ready(
  function () {
    if (Drupal.settings.ec_cart.empty) {
      $('#block-ec_cart-0').hide();
    }
    if (Drupal.settings.ec_cart.load_cart) {
      $.getJSON(Drupal.settings.ec_cart.load_path, Drupal.ec_cart_load)
    }
    $('a.cart-link').click(Drupal.ec_cart_convert_link);
  }
);
