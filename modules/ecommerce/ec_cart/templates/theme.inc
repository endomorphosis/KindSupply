<?php
// $Id: theme.inc,v 1.1.2.2 2009/03/17 12:25:59 gordon Exp $

/**
 * @file
 * Provide preprocess functions for all the theme templates
 */

/**
 * Create preprocessed variables for ec_cart_empty().
 */
function template_preprocess_ec_cart_empty(&$variables) {
  $variables['empty_text'] = t('Your shopping cart is empty. You may !start_shopping.', array('!start_shopping' => l(t('start shopping'), variable_get('ec_goto_cart_empty', 'products'))));
}

/**
 * Provide preprocessing for the cart view form
 */
function template_preprocess_ec_cart_view_form(&$variables) {
  $form =& $variables['form'];
  $variables['items'] = array();

  foreach (element_children($form['products']) as $nid) {
    $variables['items'][$nid] = array(
      'title' => drupal_render($form['products'][$nid]['title']),
      'price' => drupal_render($form['products'][$nid]['price']),
      'qty' => drupal_render($form['products'][$nid]['qty']),
      'total' => drupal_render($form['products'][$nid]['total']),
      'ops' => drupal_render($form['products'][$nid]['ops']),
    );
    
    $extra = drupal_render($form['products'][$nid]);
    if ($extra) {
      $variables['items'][$nid]['extra'] = $extra;
      $has_extra = TRUE;
    }
  }

  $variables['total'] = drupal_render($form['total']);

  $variables['output'] = drupal_render($form);

  if (isset($has_extra) && $has_extra) {
    $variables['template_files'] = 'ec-cart-view-form-extra';
  }

  drupal_add_js('misc/tableheader.js', 'core');
}

/**
 * Provide variables for the cart block
 */
function template_preprocess_ec_cart_display_block(&$variables) {
  drupal_add_css(drupal_get_path('module', 'ec_cart') .'/ec_cart.css');

  if (!$variables['cart']) {
    $variables['cart_link'] = l(t('View your cart'), 'cart/view');
    $variables['template_files'][] = 'ec-cart-display-block-cached';
  }
  else {
    $variables['item_count'] = t('%items in !your_cart', array('%items' => format_plural(count($variables['items']), '1 item', '@count items'), '!your_cart' => l(t('Your cart'), 'cart/view')));
    $variables['total'] = format_currency(ec_cart_get_total($items));

    if (!empty($variables['items'])) {
      $items = array();
      foreach ($variables['items'] as $item) {
        $items[$item->nid] = array(
          'nid' => $item->nid,
          'title' => $item->title,
          'qty' => ec_product_has_quantity($item) ? ' x '. $item->qty : '',
          'price' => format_currency(ec_cart_get_item_total($item)),
          'path' => 'node/'. $item->nid,
          'link' => l($item->title, 'node/'. $item->nid),
          'item' => $item,
        );
      }
      $variables['items'] = $items;
      $variables['checkout'] =  t('Ready to <a href="!checkout-url">checkout</a>?', array('!checkout-url' => url('checkout/cart')));
    }
    else {
      $variables['template_files'][] = 'ec-cart-display-block-empty';
    }
  }
}

