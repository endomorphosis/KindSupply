<div class="item-count">
  <?php echo $item_count; ?>
</div>
<div class="cart-items">
<?php foreach ($items as $item) { ?>
  <div id="cart-item-<?php echo $item['nid']; ?>" class="cart-item-wrapper clear-block">
    <span class="cart-item">
      <?php echo $item['link'] . $item['qty']; ?>
    </span>
    <span class="cart-line-total">
      <?php echo $item['price']; ?>
    </span>
  </div>
<?php } ?>
  <div class="total-wrapper clear-block">
    <span class="total">
      <?php echo $total; ?>
    </span>
  </div>
  <div class="checkout">
    <?php echo $checkout; ?>
  </div>
</div>
