<?php
// $Id: ec_receipt.checkout.inc,v 1.1.2.18 2009/03/22 05:23:19 gordon Exp $
/**
 * @file
 * Provide checkout implementation for receipting
 */

/**
 * Implementation of hook_checkout_init().
 */
function ec_receipt_checkout_init(&$form_state) {
  $txn =& $form_state['storage']['txn'];

  $txn->currency = variable_get('ec_default_currency', NULL);

  if (empty($form_state['storage']['rtypes'])) {
    $form_state['storage']['rtypes'] = ec_receipt_type_filter(array(
      'currencies_supported' => $txn->currency,
      'allow_payments' => TRUE,
    ));
  }

  if (isset($_REQUEST['payment_method']) && isset($form_state['storage']['rtypes'][$_REQUEST['payment_method']])) {
    $txn->payment_method = $_REQUEST['payment_method'];
  }
  else {
    $types = array_keys($form_state['storage']['rtypes']);
    $txn->payment_method = $types[0];
  }
}

/**
 * Implementation of hook_checkout_form().
 */
function ec_receipt_checkout_form(&$form, &$form_state) {
  $txn =& $form_state['storage']['txn'];

  $form['receipt_no'] = array(
    '#type' => 'value',
    '#value' => '',
  );

  if (ec_store_transaction_calc_gross($txn)) {
    if (count($form_state['storage']['rtypes']) >= 1) {
      $options = array();

      foreach ($form_state['storage']['rtypes'] as $type => $info) {
        $options[$type] = $info->name;
      }

      $form['ec_receipt']['buttons'] = array(
        '#prefix' => '<div id="payment-buttons" class="ec-inline-form">',
        '#suffix' => '</div>',
        '#weight' => -9,
      );
      $form['ec_receipt']['buttons']['select_payment_type'] = array(
        '#type' => 'select',
        '#title' => t('Select payment method'),
        '#default_value' => $txn->payment_method,
        '#options' => $options,
        '#access' => count($options) >= 1,
      );
      $form['ec_receipt']['buttons']['receipt_submit'] = array(
        '#type' => 'submit',
        '#value' => t('Select payment method'),
        '#access' => count($options) >= 1,
        '#submit' => array('ec_receipt_checkout_alter_rtype'),
        '#ahah' => array(
          'path' => 'checkout/ajax/rtype',
          'wrapper' => 'ec-receipt-form',
          'method' => 'replace',
        ),
      );
    }

    $form['ec_receipt']['ec_receipt_name'] = array(
      '#value' => ec_receipt_checkout_types(ec_receipt_get_types('type', $txn->payment_method)),
      '#weight' => -8,
      '#prefix' => '<div class="rtype-'. str_replace(array('][', '_', ' '), '-', $txn->payment_method) .'">',
      '#suffix' => '</div>',
    );
    if ($pform = ec_receipt_invoke($txn->payment_method, 'payment_form', 'transaction', $txn)) {
      $form['ec_receipt']['payment_form'] = array(
        '#tree' => TRUE,
      );
      $form['ec_receipt']['payment_form']+= $pform;
    }
    $form['ec_receipt']['#theme'] = 'ec_receipt_review_form';
  }
}

/**
 * Implementation of hook_checkout_validate().
 */
function ec_receipt_checkout_post_update(&$form, &$form_state) {
  $txn =& $form_state['storage']['txn'];
  if (isset($form_state['clicked_button']['#create_txn']) && $form_state['clicked_button']['#create_txn'] && !form_get_errors() && ec_store_transaction_calc_gross($txn)) {
    $txn->payment_data = $form_state['values']['payment_form'];

    $txn->receipt_no = ec_receipt_payment_process('transaction', $txn);
  }
}

/**
 * Handle changing of receipt type
 */
function ec_receipt_checkout_alter_rtype(&$form, &$form_state) {
  $txn =& $form_state['storage']['txn'];

  $txn->payment_method = $form_state['values']['select_payment_type'];
}

/**
 * AHAH callback to change the payment method
 */
function ec_receipt_checkout_submit_rtype_js() {
  $cached_form_state = array();
  $files = array();

  // Load the form from the Form API cache.
  if (!($cached_form = form_get_cache($_POST['form_build_id'], $cached_form_state)) || !isset($cached_form_state['storage']['txn'])) {
    form_set_error('form_token', t('Validation error, please try again. If this error persists, please contact the site administrator.'));
    $output = theme('status_messages');
    print drupal_to_js(array('status' => TRUE, 'data' => $output));
    exit();
  }

  $form_state = array('values' => $_POST) + $cached_form_state;
  $txn =& $form_state['storage']['txn'];
  $txn->payment_method = $form_state['values']['select_payment_type'];

  $form = array();

  ec_receipt_checkout_form($form, $form_state);
  $cached_form['ec_receipt'] = $form['ec_receipt'];
  $cached_form_state['storage'] = $form_state['storage'];

  $new_form['ec_receipt'] = $form['ec_receipt'];
  $form = $new_form;
  unset($form['ec_receipt']['#theme']);

  form_set_cache($_POST['form_build_id'], $cached_form, $cached_form_state);

  // Render the form for output.
  $form += array(
    '#post' => $_POST,
    '#programmed' => FALSE,
    '#tree' => FALSE,
    '#parents' => array(),
  );
  drupal_alter('form', $form, array(), 'ec_receipt_checkout_form');
  $form_state = array('submitted' => FALSE);
  $form = form_builder('ec_receipt_checkout_form', $form, $form_state);
  $output = theme('status_messages') . drupal_render($form);

  // We send the updated file attachments form.
  // Don't call drupal_json(). ahah.js uses an iframe and
  // the header output by drupal_json() causes problems in some browsers.
  print drupal_to_js(array('status' => TRUE, 'data' => $output));
  exit;
}

/**
 * Implementation of hook_checkout_submit().
 */
function ec_receipt_checkout_post_submit(&$txn, &$form_state) {
  if (ec_store_transaction_calc_gross($txn)) {
    $alloc[] = array(
      'type' => 'transaction',
      'id' => $txn->txnid,
    );
    if (empty($txn->receipt_no)) {
      return ec_receipt_payment_goto('transaction', $txn, $alloc);
    }
    else {
      if ($receipt = ec_receipt_load($txn->receipt_no)) {
        ec_receipt_allocate($receipt, $alloc);
      }
    }
    if (empty($form_state['redirect'])) {
      return variable_get('ec_receipt_return_url', 'node');
    }
  }
}

