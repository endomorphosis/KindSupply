<?php
// $Id: ec_receipt.views_default.inc,v 1.5.2.3 2009/03/19 21:42:15 darrenoh Exp $
/**
 * @file
 * Default views for the store module
 */

/**
 * Implementation of hook_views_default_views().
 */
function ec_receipt_views_default_views() {
  $views = array();

  $view = new view;
  $view->name = 'ec_receipt_list';
  $view->description = 'List of all receipts';
  $view->tag = 'ecommerce';
  $view->view_php = '';
  $view->base_table = 'ec_receipt';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to TRUE to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'erid' => array(
      'label' => 'Id',
      'exclude' => 0,
      'id' => 'erid',
      'table' => 'ec_receipt',
      'field' => 'erid',
      'relationship' => 'none',
    ),
    'customer_name' => array(
      'label' => 'Customer name',
      'exclude' => 0,
      'id' => 'customer_name',
      'table' => 'ec_customer',
      'field' => 'customer_name',
      'relationship' => 'none',
    ),
    'type' => array(
      'label' => 'Type',
      'exclude' => 0,
      'id' => 'type',
      'table' => 'ec_receipt',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'currency' => array(
      'label' => 'Currency',
      'exclude' => 0,
      'id' => 'currency',
      'table' => 'ec_receipt',
      'field' => 'currency',
      'relationship' => 'none',
    ),
    'amount' => array(
      'label' => 'Amount',
      'exclude' => 0,
      'id' => 'amount',
      'table' => 'ec_receipt',
      'field' => 'amount',
      'relationship' => 'none',
    ),
    'balance' => array(
      'label' => 'Balance',
      'exclude' => 0,
      'id' => 'balance',
      'table' => 'ec_receipt',
      'field' => 'balance',
      'relationship' => 'none',
    ),
    'changed' => array(
      'label' => 'Changed',
      'date_format' => 'custom',
      'custom_date_format' => 'j M y',
      'exclude' => 0,
      'id' => 'changed',
      'table' => 'ec_receipt',
      'field' => 'changed',
      'relationship' => 'none',
    ),
    'created' => array(
      'label' => 'Created',
      'date_format' => 'custom',
      'custom_date_format' => 'j M y',
      'exclude' => 0,
      'id' => 'created',
      'table' => 'ec_receipt',
      'field' => 'created',
      'relationship' => 'none',
    ),
    'status' => array(
      'label' => 'Status',
      'exclude' => 0,
      'id' => 'status',
      'table' => 'ec_receipt',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'operations' => array(
      'id' => 'operations',
      'table' => 'ec_receipt',
      'field' => 'operations',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'store admin report',
  ));
  $handler->override_option('title', 'Receipts');
  $handler->override_option('items_per_page', 50);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'bulk');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 1,
    'order' => 'asc',
    'columns' => array(
      'erid' => 'erid',
      'customer_name' => 'customer_name',
      'type' => 'type',
      'currency' => 'currency',
      'amount' => 'amount',
      'balance' => 'balance',
      'changed' => 'changed',
      'created' => 'created',
      'status' => 'status',
    ),
    'info' => array(
      'erid' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'customer_name' => array(
        'separator' => '',
      ),
      'type' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'currency' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'amount' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'balance' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'changed' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'created' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'status' => array(
        'sortable' => 1,
        'separator' => '',
      ),
    ),
    'default' => 'erid',
    'use_batch' => 1,
    'display_type' => '0',
    'selected_operations' => array(
      md5('ec_receipt_action_delete') => md5('ec_receipt_action_delete'),
      md5('ec_receipt_action_allocate') => md5('ec_receipt_action_allocate'),
      md5('ec_receipt_action_refund') => md5('ec_receipt_action_refund'),
    ),
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'admin/store/receipts');
  $handler->override_option('menu', array(
    'type' => 'normal',
    'title' => 'Receipts',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'weight' => 0,
  ));
  $views[$view->name] = $view;

  $view = new view;
  $view->name = 'customer_receipt_list';
  $view->description = 'List of receipts by customer';
  $view->tag = 'ecommerce';
  $view->view_php = '';
  $view->base_table = 'ec_receipt';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to TRUE to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'erid' => array(
      'label' => 'Receipt no.',
      'exclude' => 0,
      'id' => 'erid',
      'table' => 'ec_receipt',
      'field' => 'erid',
      'relationship' => 'none',
    ),
    'type' => array(
      'label' => 'Type',
      'exclude' => 0,
      'id' => 'type',
      'table' => 'ec_receipt',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'amount' => array(
      'label' => 'Amount',
      'exclude' => 0,
      'id' => 'amount',
      'table' => 'ec_receipt',
      'field' => 'amount',
      'relationship' => 'none',
    ),
    'balance' => array(
      'label' => 'Balance',
      'exclude' => 0,
      'id' => 'balance',
      'table' => 'ec_receipt',
      'field' => 'balance',
      'relationship' => 'none',
    ),
    'created' => array(
      'label' => 'Created',
      'date_format' => 'custom',
      'custom_date_format' => 'j M y',
      'exclude' => 0,
      'id' => 'created',
      'table' => 'ec_receipt',
      'field' => 'created',
      'relationship' => 'none',
    ),
    'status' => array(
      'label' => 'Status',
      'exclude' => 0,
      'id' => 'status',
      'table' => 'ec_receipt',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'operations' => array(
      'label' => 'Operations',
      'exclude' => 0,
      'id' => 'operations',
      'table' => 'ec_receipt',
      'field' => 'operations',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'ecid' => array(
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '%1',
      'default_argument_type' => 'user',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'break_phrase' => 1,
      'not' => 0,
      'id' => 'ecid',
      'table' => 'ec_customer',
      'field' => 'ecid',
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'deadwood_category' => 0,
        'deadwood_item' => 0,
        'amazon_book' => 0,
        'file' => 0,
        'page' => 0,
        'product' => 0,
        'product_some' => 0,
        'story' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(),
      'validate_argument_type' => 'tid',
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'ec_customer',
  ));
  $handler->override_option('use_ajax', TRUE);
  $handler->override_option('items_per_page', 50);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 1,
    'order' => 'desc',
    'columns' => array(
      'erid' => 'erid',
      'type' => 'type',
      'amount' => 'amount',
      'balance' => 'balance',
      'created' => 'created',
      'status' => 'status',
    ),
    'info' => array(
      'erid' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'type' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'amount' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'balance' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'created' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'status' => array(
        'sortable' => 1,
        'separator' => '',
      ),
    ),
    'default' => 'erid',
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'user/%/store/receipts');
  $handler->override_option('menu', array(
    'type' => 'tab',
    'title' => 'Receipts',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'weight' => 0,
  ));
  $views[$view->name] = $view;

  $view = new view;
  $view->name = 'ec_receipt_allocation_list';
  $view->description = 'List of Allocations made for a receipt';
  $view->tag = 'ecommerce';
  $view->view_php = '';
  $view->base_table = 'ec_receipt_allocation';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to TRUE to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'type' => array(
      'id' => 'type',
      'table' => 'ec_receipt_allocation',
      'field' => 'type',
    ),
    'eaid' => array(
      'label' => 'Id',
      'exclude' => 0,
      'id' => 'eaid',
      'table' => 'ec_receipt_allocation',
      'field' => 'eaid',
      'relationship' => 'none',
    ),
    'created' => array(
      'label' => 'Created',
      'date_format' => 'custom',
      'custom_date_format' => 'j M y',
      'exclude' => 0,
      'id' => 'created',
      'table' => 'ec_receipt_allocation',
      'field' => 'created',
      'relationship' => 'none',
    ),
    'amount' => array(
      'label' => 'Amount',
      'exclude' => 0,
      'id' => 'amount',
      'table' => 'ec_receipt_allocation',
      'field' => 'amount',
      'relationship' => 'none',
    ),
    'reversed' => array(
      'label' => 'Reversed',
      'type' => 'yes-no',
      'not' => 0,
      'exclude' => 0,
      'id' => 'reversed',
      'table' => 'ec_receipt_allocation',
      'field' => 'reversed',
      'relationship' => 'none',
    ),
    'operations' => array(
      'label' => 'Operations',
      'exclude' => 0,
      'id' => 'operations',
      'table' => 'ec_receipt_allocation',
      'field' => 'operations',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'erid' => array(
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'default_argument_type' => 'receipt_id',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'empty',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'erid',
      'table' => 'ec_receipt_allocation',
      'field' => 'erid',
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'deadwood_category' => 0,
        'deadwood_item' => 0,
        'amazon_book' => 0,
        'file' => 0,
        'page' => 0,
        'product' => 0,
        'product_some' => 0,
        'story' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(),
      'validate_argument_type' => 'tid',
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('empty', 'No allocations');
  $handler->override_option('empty_format', '1');
  $handler->override_option('use_ajax', TRUE);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 1,
    'order' => 'asc',
    'columns' => array(
      'type' => 'type',
      'eaid' => 'eaid',
      'created' => 'created',
      'amount' => 'amount',
      'reversed' => 'reversed',
    ),
    'info' => array(
      'type' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'eaid' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'created' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'amount' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'reversed' => array(
        'sortable' => 1,
        'separator' => '',
      ),
    ),
    'default' => 'created',
  ));
  $views[$view->name] = $view;

  return $views;
}

