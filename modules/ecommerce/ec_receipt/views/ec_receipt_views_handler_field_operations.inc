<?php
// $Id: ec_receipt_views_handler_field_operations.inc,v 1.1 2008/10/23 03:02:31 gordon Exp $
/**
 * @file
 * Provide list of operation links for a receipt
 */

class ec_receipt_views_handler_field_operations extends views_handler_field {

  function render($values) {
    if ($receipt = ec_receipt_load($values->{$this->field_alias})) {
      return theme('links', module_invoke_all('link', 'ec_receipt', $receipt));
    }
  }
}
