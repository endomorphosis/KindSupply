<?php
// $Id: ec_receipt.rules.inc,v 1.1.2.1 2008/11/03 12:37:41 gordon Exp $
/**
 * @file
 * Implement Rules and actions for Receipts
 */

/**
 * Implementation of hook_rules_event_info().
 */
function ec_receipt_rules_event_info() {
  return array(
    'ec_receipt_saved' => array(
      'label' => t('Receipt has been saved.'),
      'module' => 'eC Receipts',
      'arguments' => array(
        'receipt' => array('type' => 'receipt', 'label' => t('Receipt')),
        'orig_receipt' => array('type' => 'receipt', 'label' => t('Original Receipt')),
      ),
    ),
  );
}

/**
 * Implementation of hook_rules_condition_info().
 */
function ec_receipt_rules_condition_info() {
  return array(
    'ec_receipt_condition_receipt_status' => array(
      'label' => t('Receipt status'),
      'arguments' => array(
        'receipt' => array('type' => 'receipt', 'label' => t('Receipt')),
      ),
      'module' => 'eC Receipts',
    ),
  );
}

function ec_receipt_condition_receipt_status() {
}
