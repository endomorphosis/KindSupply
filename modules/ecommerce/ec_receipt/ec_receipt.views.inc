<?php
// $Id: ec_receipt.views.inc,v 1.3.2.1 2009/03/25 03:03:27 gordon Exp $
/**
 * @file
 * Implemention of views for Receipts
 */

/**
 * Implementation of hook_views_data().
 */
function ec_receipt_views_data() {
  $data = array();

  $data['ec_receipt']['table']['group'] = t('Receipts');

  $data['ec_receipt']['table']['base'] = array(
    'field' => 'erid',
    'title' => t('Receipt'),
    'help' => t('A unique identifier for each receipt'),
  );

  $data['ec_receipt']['table']['join'] = array(
    'ec_receipt_allocation' => array(
      'left_field' => 'erid',
      'field' => 'erid',
      'type' => 'INNER',
    ),
  );

  $data['ec_receipt']['erid'] = array(
    'title' => t('Receipt no.'),
    'help' => t('Provides Unique identifier for each receipt'),
    'field' => array(
      'field' => 'erid',
      'click sortable' => TRUE,
    ),
  );
  $data['ec_receipt']['type'] = array(
    'title' => t('Type'),
    'help' => t('Provides the type of receipt'),
    'field' => array(
      'handler' => 'ec_receipt_views_handler_field_type',
      'click sortable' => TRUE,
    ),
  );
  $data['ec_receipt']['currency'] = array(
    'title' => t('Currency'),
    'help' => t('Provides the currency of the receipt'),
    'field' => array(
      'field' => 'currency',
      'click sortable' => TRUE,
    ),
  );
  $data['ec_receipt']['amount'] = array(
    'title' => t('Amount'),
    'help' => t('Provides the amount of the receipt'),
    'field' => array(
      'handler' => 'ec_common_views_handler_field_format_currency',
      'click sortable' => TRUE,
    ),
  );
  $data['ec_receipt']['allocated'] = array(
    'title' => t('Allocated'),
    'help' => t('Provides the amount allocated of the receipt'),
    'field' => array(
      'handler' => 'ec_common_views_handler_field_format_currency',
      'click sortable' => TRUE,
    ),
  );
  $data['ec_receipt']['balance'] = array(
    'title' => t('Balance'),
    'help' => t('Provides the balance of the receipt'),
    'field' => array(
      'handler' => 'ec_common_views_handler_field_format_currency',
      'click sortable' => TRUE,
    ),
  );
  $data['ec_receipt']['status'] = array(
    'title' => t('Status'),
    'help' => t('Provides the status of the receipt'),
    'field' => array(
      'handler' => 'ec_receipt_views_handler_field_status',
      'click sortable' => TRUE,
    ),
  );
  $data['ec_receipt']['created'] = array(
    'title' => t('Created'),
    'help' => t('Provides the created date of the receipt'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
  );
  $data['ec_receipt']['changed'] = array(
    'title' => t('Changed'),
    'help' => t('Provides the last change date of the receipt'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
  );
  $data['ec_receipt']['operations'] = array(
    'title' => t('Operations'),
    'help' => t('Provides links for doing other functions'),
    'field' => array(
      'handler' => 'ec_receipt_views_handler_field_operations',
      'real field' => 'erid',
    ),
  );

  $data['ec_receipt_allocation']['table']['group'] = t('Receipt allocation');

  $data['ec_receipt_allocation']['table']['base'] = array(
    'field' => 'eaid',
    'title' => t('Receipt Allocation'),
    'help' => t('Show allocation of Receipts'),
  );

  $data['ec_receipt_allocation']['table']['join'] = array(
    'ec_receipt' => array(
      'left_field' => 'erid',
      'field' => 'erid',
      'type' => 'LEFT',
    ),
  );

  $data['ec_receipt_allocation']['eaid'] = array(
    'title' => t('Allocation id'),
    'help' => t('Provides the unique identifier for each allocation'),
    'field' => array(
      'field' => 'eaid',
      'click sortable' => TRUE,
    ),
  );
  $data['ec_receipt_allocation']['erid'] = array(
    'title' => t('Receipt Id'),
    'help' => t('Provides an argument for allocations by receipt id.'),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  $data['ec_receipt_allocation']['type'] = array(
    'title' => t('Type'),
    'help' => t('Provides the type of object that the receipt was allocated too'),
    'field' => array(
      'handler' => 'ec_receipt_views_handler_field_allocation_type',
      'click sortable' => TRUE,
    ),
  );
  $data['ec_receipt_allocation']['etid'] = array(
    'title' => t('External id'),
    'help' => t('Provides th external id of the object type the receipt was allocated to.'),
    'field' => array(
      'field' => 'etid',
      'click sortable' => TRUE,
    ),
  );
  $data['ec_receipt_allocation']['created'] = array(
    'title' => t('Created'),
    'help' => t('Provides the created date of the receipt allocation'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
  );
  $data['ec_receipt_allocation']['amount'] = array(
    'title' => t('Amount'),
    'help' => t('Provides the amount of the receipt allocation'),
    'field' => array(
      'handler' => 'ec_common_views_handler_field_format_currency',
      'click sortable' => TRUE,
    ),
  );
  $data['ec_receipt_allocation']['reversed'] = array(
    'title' => t('Reversed'),
    'help' => t('Provides reversed flag status'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
  );
  $data['ec_receipt_allocation']['operations'] = array(
    'title' => t('Operations'),
    'help' => t('Provides links for doing other functions'),
    'field' => array(
      'handler' => 'ec_receipt_views_handler_field_allocation_operations',
      'real field' => 'eaid',
    ),
  );

  if (module_exists('search')) {
    $data['search_index']['table']['join'] = array(
      'ec_receipt' => array(
        'left_field' => 'erid',
        'field' => 'sid',
      ),
    );

    $data['search_total']['table']['join'] = array(
      'ec_receipt' => array(
        'left_table' => 'search_index',
        'left_field' => 'word',
        'field' => 'word',
      ),
    );

    $data['search_dataset']['table']['join'] = array(
      'ec_receipt' => array(
        'left_table' => 'search_index',
        'left_field' => 'sid',
        'field' => 'sid',
        'extra' => 'search_index.type = search_dataset.type',
        'type' => 'INNER',
      ),
    );
  }

  return $data;
}

/**
 * Implementation of hook_views_plugins().
 */
function ec_receipt_views_plugins() {
  return array(
    'module' => 'ec_receipt',
    'argument default' => array(
      'receipt_id' => array(
        'title' => t('Receipt Id from URL'),
        'handler' => 'ec_receipt_views_plugin_argument_default_receipt',
        'path' => drupal_get_path('module', 'ec_receipt') .'/views',
        'parent' => 'fixed',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_handler().
 */
function ec_receipt_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'ec_receipt') .'/views',
    ),
    'handlers' => array(
      'ec_receipt_views_handler_field_type' => array(
        'parent' => 'views_handler_field',
      ),
      'ec_receipt_views_handler_field_status' => array(
        'parent' => 'views_handler_field',
      ),
      'ec_receipt_views_handler_field_operations' => array(
        'parent' => 'views_handler_field',
      ),
      'ec_receipt_views_handler_field_allocation_type' => array(
        'parent' => 'views_handler_field',
      ),
      'ec_receipt_views_handler_field_allocation_operations' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

