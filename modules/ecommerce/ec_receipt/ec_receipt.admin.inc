<?php
// $Id: ec_receipt.admin.inc,v 1.4.2.9 2009/03/26 21:19:12 gordon Exp $

/**
 * @file
 * All Receipt admin functions
 */

/**
 * Refund Receipt to Customer
 */
function ec_receipt_refund($erid = NULL) {
  if (empty($erid) || !($receipt = ec_receipt_load($erid))) {
    drupal_not_found();
    exit;
  }

  $form = ec_receipt_view_form($receipt);


  return confirm_form($form, t('Are you sure you want to refund this receipt?'), isset($_REQUEST['destination']) ? $_REQUEST['destination'] : 'admin/store/receipts', t('This will refund the money to the customer'), t('Refund'));
}

function ec_receipt_refund_submit(&$form, &$form_state) {
  $receipt =& $form_state['values']['receipt'];
  $alloc[] = array(
    'type' => 'refund',
    'id' => $receipt->erid,
  );
  ec_receipt_allocate($receipt, $alloc);
  $type = ec_receipt_get_types('type', $receipt->type);

  if (isset($type->allow_refunds) && $type->allow_refunds) {
    ec_receipt_invoke($receipt->type, 'refund', $receipt);
  }
  else {
    drupal_set_message(t('Receipt %erid has been marked as Refund Pending. Once this receipt has been refunded, please mark this receipt as Refunded.', array('%erid' => $receipt->erid)));
  }

  $form_state['redirect'] = array('admin/store/receipts', 'erid='. $receipt->erid);
}

/**
 * Administration for receipt types
 */
function ec_receipt_admin_rtypes_form() {
  drupal_add_css(drupal_get_path('module', 'ec_receipt') .'/ec_receipt_admin.css');
  $rtypes = ec_receipt_get_types();
  $form = array();
  $form['types'] = array(
    '#tree' => TRUE,
  );
  if (!empty($rtypes)) {
    foreach ($rtypes as $type => $info) {
      if (empty($info->internal) || !$info->internal) {
        $form['types'][$type]['type'] = array(
          '#type' => 'value',
          '#value' => $info->type,
        );
        $form['types'][$type]['name'] = array(
          '#value' => l($info->name, 'admin/ecsettings/rtypes/'. $type),
        );
        $form['types'][$type]['icon'] = array(
          '#value' => isset($info->icon) ? theme('ec_receipt_icon', $info->icon) : '',
        );
        $form['types'][$type]['description'] = array(
          '#value' => $info->description,
        );
        $form['types'][$type]['allow_payments'] = array(
          '#value' => isset($info->allow_payments) ? ($info->allow_payments ? t('Yes') : t('No')) : '<em>'. t('N/A') .'</em>',
        );
        $form['types'][$type]['allow_admin_payments'] = array(
          '#value' => isset($info->allow_admin_payments) ? ($info->allow_admin_payments ? t('Yes') : t('No')) : '<em>'. t('N/A') .'</em>',
        );
        $form['types'][$type]['allow_refunds'] = array(
          '#value' => isset($info->allow_refunds) ? ($info->allow_refunds ? t('Yes') : t('No')) : '<em>'. t('N/A') .'</em>',
        );
        $form['types'][$type]['allow_payto'] = array(
          '#value' => isset($info->allow_payto) ? ($info->allow_payto ? t('Yes') : t('No')) : '<em>'. t('N/A') .'</em>',
        );
        $form['types'][$type]['allow_recurring'] = array(
          '#value' => isset($info->allow_recurring) ? ($info->allow_recurring ? t('Yes') : t('No')) : '<em>'. t('N/A') .'</em>',
        );
        $form['types'][$type]['weight'] = array(
          '#type' => 'weight',
          '#default_value' => !empty($info->weight) ? $info->weight : 0,
        );
        /**
        if (isset($info->gateway_requirements)) {
          $form['types'][$type]['gateway_requirements'] = array(
            '#type' => 'item',
            '#title' => t('Other requirements'),
            '#value' => is_array($info->gateway_requirements) ? theme('item_list', $info->gateway_requirements) : $info->gateway_requirements,
          );
        } */
      }
    }
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update'),
    );
  }
  else {
    drupal_set_message(t('No payment gateways have been enabled'), 'error');
  }
  return $form;
}

function ec_receipt_admin_rtypes_form_submit(&$form, &$form_state) {
  foreach (element_children($form_state['values']['types']) as $type) {
    if ($info = ec_receipt_get_types('type', $type)) {
      ec_receipt_type_save((object)$form_state['values']['types'][$type]);
    }
    else {
      ec_receipt_type_delete($type);
    }
  }
}

/**
 * ec_receipt settings
 */
function ec_receipt_settings() {
  $form = array();

  $options = array('' => '--');
  $options += ec_receipt_currency_list();

  $form['ec_default_currency'] = array(
    '#type'           => 'select',
    '#title'          => t('Default currency'),
    '#default_value'  => variable_get('ec_default_currency', ''),
    '#options'        => $options,
    '#required'       => TRUE,
    '#description'    => t('Currencies are dependent on payment gateways. If there appear to be no currencies available, ensure at least one "Payment Methods" module is enabled.')
  );

  $form['ec_receipt_valid_cards'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Valid Card Types'),
    '#default_value' => variable_get('ec_receipt_valid_cards', array()),
    '#options' => ec_receipt_card_types(TRUE),
    '#description' => t('Select card types which will be accepted. Note that not all payment gateways allow this functionality.'),
  );

  $form['ec_receipt_return_url'] =  array(
    '#type' => 'textfield',
    '#title' => t('Successful payment URL'),
    '#default_value' => variable_get('ec_receipt_return_url', 'node'),
    '#size' => 70,
    '#maxlength' => 180,
    '#description' => t('This is the destination to which you would like to send your customers when their payment has been successfully completed. The URL must be a Drupal system path. If you are not using clean URLs, specify the part after \'?q=\'. If unsure, specify \'node\'. You may also enter \'%order-history\' to link to the user\'s order history.')
  );

  $form['ec_receipt_cancel_url'] =  array(
    '#type' => 'textfield',
    '#title' => t('Cancel payment URL'),
    '#default_value' => variable_get('ec_receipt_cancel_url', 'node'),
    '#size' => 70,
    '#maxlength' => 180,
    '#description' => t('This is the destination to which you would like to send your customers if they cancel their payment. The URL must be a Drupal system path. If you are not using clean URLs, specify the part after "?q=". If unsure, specify "node".')
  );

  return system_settings_form($form);
}

/**
 * Edit Receipt types
 */
function ec_receipt_admin_type_form(&$form_state, $type) {
  $info = ec_receipt_get_types('type', $type);
  $form = array();
  $form['type'] = array(
    '#type' => 'value',
    '#value' => $info->type,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $info->name,
    '#description' => t('This is the name of the Payment gateway which will be shown to your customers'),
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $info->description,
    '#description' => t('Description of the payment gateway as the customer will see it.'),
  );
  $form['allow_payments'] = array(
    '#type' => 'select',
    '#title' => t('Allow payments'),
    '#default_value' => isset($info->allow_payments) ? 1 : 0,
    '#options' => isset($info->allow_payments) ? array(t('No'), t('Yes')) : array(t('N/A')),
    '#disabled' => isset($info->allow_payments) ? FALSE : TRUE,
    '#description' => t('Disable/Enable Payments by customers'),
  );
  $form['allow_admin_payments'] = array(
    '#type' => 'select',
    '#title' => t('Allow 3rd party payments'),
    '#default_value' => isset($info->allow_admin_payments) ? 1 : 0,
    '#options' => isset($info->allow_admin_payments) ? array(t('No'), t('Yes')) : array(t('N/A')),
    '#disabled' => isset($info->allow_admin_payments) ? FALSE : TRUE,
    '#description' => t('Disable/Enable Payments by customers'),
  );
  $form['allow_refunds'] = array(
    '#type' => 'select',
    '#title' => t('Allow refunds'),
    '#default_value' => isset($info->allow_refunds) ? 1 : 0,
    '#options' => isset($info->allow_refunds) ? array(t('No'), t('Yes')) : array(t('N/A')),
    '#disabled' => isset($info->allow_refunds) ? FALSE : TRUE,
    '#description' => t('Disable/Enable Payments by customers'),
  );
  $form['allow_payto'] = array(
    '#type' => 'select',
    '#title' => t('Allow payments to customers'),
    '#default_value' => isset($info->allow_payto) ? 1 : 0,
    '#options' => isset($info->allow_payto) ? array(t('No'), t('Yes')) : array(t('N/A')),
    '#disabled' => isset($info->allow_pato) ? FALSE : TRUE,
    '#description' => t('Disable/Enable Payments by customers'),
  );
  $form['allow_recurring'] = array(
    '#type' => 'select',
    '#title' => t('Allow recurring payments'),
    '#default_value' => isset($info->allow_recurring) ? 1 : 0,
    '#options' => isset($info->allow_recurring) ? array(t('No'), t('Yes')) : array(t('N/A')),
    '#disabled' => isset($info->allow_recurring) ? FALSE : TRUE,
    '#description' => t('Disable/Enable Payments by customers'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update receipt type'),
  );
  return $form;
}

function ec_receipt_admin_type_form_submit(&$form, &$form_state) {
  ec_receipt_type_save((object)$form_state['values']);
}

/**
 * Administer Allocation types
 */
function ec_receipt_admin_atypes_form() {
  $form = array();

  $form['types'] = array(
    '#tree' => TRUE,
  );
  $types = ec_receipt_get_atypes();

  foreach ($types as $type => $info) {
    $form['types'][$type]['type'] = array(
      '#value' => $info->type,
    );
    $form['types'][$type]['name'] = array(
      '#value' => $info->name,
    );
    $form['types'][$type]['description'] = array(
      '#value' => $info->description,
    );
  }

  return $form;
}

/**
 * Receipt View form
 */
function ec_receipt_view_form(&$form_state, $receipt, $links = FALSE) {
  drupal_add_css(drupal_get_path('module', 'ec_receipt') .'/ec_receipt.css');

  $form['receipt'] = array(
    '#type' => 'value',
    '#value' => $receipt,
  );

  $form['erid'] = array(
    '#type' => 'item',
    '#title' => t('Receipt No'),
    '#value' => $receipt->erid,
  );

  $form['type'] = array(
    '#type' => 'item',
    '#title' => t('Type'),
    '#value' => !($type = ec_receipt_get_types('name', $receipt->type)) ? $type : $receipt->type,
  );

  $form['amounts'] = array(
    '#prefix' => '<div id="receipt-amounts">',
    '#suffix' => '</div>',
  );

  $form['amounts']['amount'] = array(
    '#type' => 'item',
    '#title' => t('Amount'),
    '#value' => format_currency($receipt->amount),
  );

  $form['amounts']['allocated'] = array(
    '#type' => 'item',
    '#title' => t('Allocated'),
    '#value' => format_currency($receipt->allocated),
  );

  $form['amounts']['balance'] = array(
    '#type' => 'item',
    '#title' => t('Balance'),
    '#value' => format_currency($receipt->balance),
  );

  $name = ec_customer_get_name($receipt->ecid);
  $view_link = ec_customer_links($receipt->ecid, 'view');

  $form['customer'] = array(
    '#type' => 'item',
    '#title' => t('Customer'),
    '#value' => !$links && empty($view_link) ? $name : l($name, $view_link[0]),
  );

  $form['alloc'] = array(
    '#type' => 'item',
    '#title' => t('Allocation'),
    '#value' => views_embed_view('ec_receipt_allocation_list'),
  );

  return $form;
}

/**
 *
 */
function ec_receipt_reverse_allocation_form($form_state, $receipt, $eaid = NULL) {
  if (empty($eaid) || !($alloc = db_fetch_object(db_query('SELECT * FROM {ec_receipt_allocation} WHERE eaid = %d', $eaid)))) {
    drupal_not_found();
    exit();
  }

  $form = array();

  $form['receipt'] = array(
    '#type' => 'value',
    '#value' => $receipt,
  );
  $form['alloc'] = array(
    '#type' => 'value',
    '#value' => $alloc,
  );

  return confirm_form($form, t('Reverse allocation for receipt %erid', array('%erid' => $receipt->erid)), 'store/receipt/view/'. $receipt->erid, NULL, t('Reverse'));
}

function ec_receipt_reverse_allocation_form_validate(&$form, &$form_state) {
  if ($object = ec_receipt_alloc_invoke($form_state['values']['alloc']->type, 'load', $form_state['values']['alloc']->etid)) {
    ec_receipt_alloc_invoke($form_state['values']['alloc']->type, 'can_reverse', $object, TRUE);
  }
}

function ec_receipt_reverse_allocation_form_submit(&$form, &$form_state) {
  ec_receipt_reverse_allocation($form_state['values']['receipt'], $form_state['values']['alloc']);
  return 'store/receipt/view/'. $form_state['values']['receipt']->erid;
}

/**
 * Reverse Allocation
 * TODO: Convert to use drupal_write_record.
 */
function ec_receipt_reverse_allocation($receipt, $alloc) {
  if ($object = ec_receipt_alloc_invoke($alloc->type, 'load', $alloc->etid)) {
    if ((ec_receipt_alloc_invoke($alloc->type, 'can_reverse', $object)) === FALSE) {
      return FALSE;
    }
    $total = ec_receipt_alloc_invoke($alloc->type, 'get_total', $object);
    $reversal = drupal_clone($alloc);
    $reversal->eaid = db_last_insert_id('ec_receipt_allocation', 'eaid');
    $reversal->created = time();
    $reversal->amount*= -1;
    $reversal->reversed = $alloc->eaid;

    $receipt->allocated+= $reversal->amount;
    $receipt->balance-= $reversal->amount;

    ec_receipt_alloc_invoke($allocate['type'], 'allocation', $object, $total - $reversal->amount);
    $sql = "INSERT INTO {ec_receipt_allocation} (eaid, erid, type, etid, created, amount, reversed) VALUES (%d, %d, '%s', '%s', %d, %d, %d)";
    db_query($sql, (array)$reversal);
    db_query('UPDATE {ec_receipt_allocation} SET reversed = %d WHERE eaid = %d', $reversal->eaid, $alloc->eaid);
    ec_receipt_save($receipt);
    return TRUE;
  }
}

