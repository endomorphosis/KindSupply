<?php
// $Id: ec_anon.install,v 1.10.2.8 2009/02/23 22:31:37 gordon Exp $

/**
 * @file
 * @author Sammy Spets thanks to Synerger Pty Ltd
 */

/**
 * Implementation of hook_schema().
 */
function ec_anon_schema() {
  $schema = array();

  $schema['ec_anon'] = array(
    'fields' => array(
       'vid' => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'disp-width' => '10'),
       'policy' => array('type' => 'int', 'size' => 'tiny', 'not null' => TRUE, 'disp-width' => '4')
    ),
    'primary key' => array('vid'),
  );

  return $schema;
}

/**
 * Installs e-Commerce anonymous purchasing tables into the database.
 */
function ec_anon_install() {
  drupal_install_schema('ec_anon');

  // Set the ec_anon module as the first checkout screen.
  $screens = array('ec_anon');
  foreach (variable_get('ec_checkout_screens', array()) as $module) {
    if ($module != 'ec_anon' && $module != 'cart') {
      $screens[] = $module;
    }
  }
  $screens[] = 'cart';
  variable_set('ec_checkout_screens', $screens);

  drupal_set_message(st('ec_anon module has been set as the first checkout screen'));
}

/**
 * Implementation of hook_uninstall().
 */
function ec_anon_uninstall() {
  drupal_uninstall_schema('ec_anon');
  variable_del('ec_anon_policy');
}

/**
 * Implementation of hook_enable().
 */
function ec_anon_enable() {
  $plist = ec_anon_policy_list(FALSE);
  drupal_set_message(st('Anonymous purchasing policy for the site has been set to <b>!policy</b>. You can change it on the <a href="@settingsurl">store settings page</a>.',
    array('!policy' => $plist[variable_get('ec_anon_policy', ECANON_POLICY_OPTIONAL)], '@settingsurl' => url('admin/ecsettings'))));
  if (module_exists('ec_product')) {
    foreach (ec_product_ptypes_get() as $ptype => $info) {
      ec_product_feature_enable($info, 'anonymous');
    }
  }

  // Set the policy variable.
  $oldval = variable_get('store_auth_cust', FALSE);
  if ($oldval === FALSE) {
    $newval = ECANON_POLICY_DEFAULT;
  }
  elseif (!empty($oldval)) {
    $newval = ECANON_POLICY_NEVER;
   }
   else {
    $newval = ECANON_POLICY_OPTIONAL;
  }
  variable_set('ec_anon_policy', $newval);
}

/**
 * Implementation of hook_disable().
 */
function ec_anon_disable() {
  if (module_exists('ec_product')) {
    foreach (ec_product_ptypes_get() as $ptype => $info) {
      ec_product_feature_disable($info, 'anonymous');
    }
  }
}

/**
 * Implementation of hook_update_n().
 */
function ec_anon_update_4001() {
  if (!function_exists('ec_anon_policy_list')) {
    drupal_load('module', 'ec_anon');
  }
  ec_anon_enable();
  return array();
}
