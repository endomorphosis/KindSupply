<?php
// $Id: ec_anon.checkout.inc,v 1.1.2.5 2009/03/09 01:51:53 gordon Exp $

/**
 * @file
 * Provide checkout support for anonymous customers
 */

/**
 * Implementation of hook_checkout_init().
 */
function ec_anon_checkout_init(&$form_state) {
  $txn =& $form_state['storage']['txn'];
  global $user;

  // User needs to login or create an account before they can checkout.
  if ($user->uid == 0 && variable_get('ec_anon_policy', ECANON_POLICY_DEFAULT) == ECANON_POLICY_NEVER) {
    drupal_set_message(t('Login or <a href="!reg-link">register</a> to continue the checkout process', array('!reg-link' => url('user/register', drupal_get_destination()))));
    drupal_goto('user', drupal_get_destination());
  }

  // If this is an anonymous user, check all the products and see if they can
  // be sold to them. If there are products which cannot be sold to an
  // anonymous user then get the user to login or register.
  foreach ($txn->items as $item) {
    if (ec_product_attributes_get($item, 'registered_user')) {
      drupal_set_message(t('Login or <a href="!reg-link">register</a> to continue the checkout process', array('!reg-link' => url('user/register', drupal_get_destination()))));
      drupal_goto('user', drupal_get_destination());
    }
  }
}
