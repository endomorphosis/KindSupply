<?php
// $Id: ec_customer_views_plugin_access_customer.inc,v 1.2 2008/10/22 12:32:04 gordon Exp $
/**
 * @file
 * Implement customer access plugin
 */

class ec_customer_views_plugin_access_customer extends views_plugin_access {
  function access($account) {
    return ec_customer_check_access('user', $account->uid);
  }

  function get_access_callback() {
    return array('ec_customer_check_access', 'user');
  }

  function summary_title() {
    return t('Customer');
  }
}

