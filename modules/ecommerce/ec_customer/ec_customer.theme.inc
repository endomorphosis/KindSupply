<?php
// $Id: ec_customer.theme.inc,v 1.1.2.6 2009/02/09 04:00:51 gordon Exp $

/**
 * @file
 * Customer theme functions
 */

function theme_ec_customer_ctypes_form($form) {
  $output = '';

  $elements = element_children($form['ec_customer_ctypes']);
  if (!empty($elements)) {
    drupal_add_tabledrag('ctype-table', 'order', 'sibling', 'ctype-weight');
    $header = array(t('Name'), t('Type'), t('Description'), t('Weight'));

    foreach ($elements as $element) {
      $rows[] = array(
        'data' => array(
          drupal_render($form['ec_customer_ctypes'][$element]['name']),
          drupal_render($form['ec_customer_ctypes'][$element]['type']),
          drupal_render($form['ec_customer_ctypes'][$element]['description']),
          drupal_render($form['ec_customer_ctypes'][$element]['weight']),
        ),
        'class' => 'draggable',
      );
    }
    $output .= theme('table', $header, $rows, array('id' => 'ctype-table'));
  }
  else {
    drupal_set_message(t('No customer modules have been enabled.'), 'error');
  }

  $output .= drupal_render($form);
  return $output;
}

/**
 * Themes the address form elements on the checkout review page
 */
function theme_ec_customer_address($form) {
  $title = drupal_render($form['title']);
  drupal_add_css(drupal_get_path('module', 'ec_customer') .'/ec_customer.css');
  return theme('box', $title, drupal_render($form));
}

function theme_ec_customer_anon_address($form) {
  $title    = drupal_render($form['title']);
  $contents = drupal_render($form);
  return theme('box', $title, $contents);
}

/**
 * Insert the email field in case the user is not logged
 *
 * @ingroup themeable
 */
function theme_ec_mail_checkout_form(&$form) {
  if (!empty($form['mail']['#value'])) {
    global $user;

    $changeurl = NULL;
    $uid = ec_customer_get_uid($form['#txn']->customer);
    if (empty($uid)) {
      $changeurl = ' '. l('('. t('Login') .')', 'user/login',
        array('class' => 'checkout-link'), 'destination=checkout/process');
    }

    return theme('box', t('Email address') . $changeurl, drupal_render($form['mail']));
  }
}
