// $Id: ec_customer.checkout.js,v 1.1.2.1 2009/02/09 04:36:21 gordon Exp $

Drupal.displayShippingAddress = function (display) {
  if (display) {
    $('.customer-addresses').removeClass('shippable');
    $('#customer-address-shipping').hide();
  }
  else {
    $('.customer-addresses').addClass('shippable');
    $('#customer-address-shipping').show();
  }
}

Drupal.behaviors.ecCustomer = function () {
  if ($('#ec-customer-use-for-shipping:checked').size()) {
    Drupal.displayShippingAddress(true);
  }
  else {
    Drupal.displayShippingAddress(false);
  }

  $('#ec-customer-use-for-shipping').click(
    function () {
      if (this.checked) {
        Drupal.displayShippingAddress(true);
      }
      else {
        Drupal.displayShippingAddress(false);
      }
      
    }
  );
}

