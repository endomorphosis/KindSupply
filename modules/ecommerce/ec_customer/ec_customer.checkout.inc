<?php
// $Id: ec_customer.checkout.inc,v 1.1.2.29 2009/03/24 01:07:22 gordon Exp $
/**
 * @file
 * Provide customer implementation for checkout
 */

/**
 * Implementation of hook_checkout_init().
 */
function ec_customer_checkout_init(&$form_state) {
  $txn =& $form_state['storage']['txn'];
  $txn->customer = ec_customer_get_customer();
  $txn->mail = ec_customer_get_email($txn->customer);

  $addresses = ec_customer_get_addresses($txn->customer);
  if (!empty($addresses)) {
    foreach ($addresses as $address) {
      $address = (array)$address;
      $form_state['storage']['addresses'][$address['aid']] = $address;
    }
  }
  else {
    $form_state['storage']['addresses'] = array();
  }

  ec_customer_prepare_addresses($txn, $form_state['storage']['addresses']);
}

/**
 * Implementation of hook_checkout_form().
 */
function ec_customer_checkout_form(&$form, &$form_state) {
  $txn =& $form_state['storage']['txn'];

  $form['ec_customer'] = array(
    '#prefix' => '<div class="customer-addresses'. (!empty($txn->shippable) ? ' shippable' : '') .'">',
    '#suffix' => '</div>',
    '#theme' => 'ec_customer_address',
  );
  $form['ec_customer']['title'] = array(
    '#value' => t(!empty($txn->shippable) ? 'Shipping and billing address' : 'Billing address'),
  );
  $form['ec_customer']['address'] = array(
    '#tree' => TRUE,
  );
  $options = array('' => t('New address'));
  
  foreach ($form_state['storage']['addresses'] as $address) {
    $address = (array)$address;
    $options[$address['aid']] = t('@firstname @lastname, @street in @city', array('@firstname' => $address['firstname'], '@lastname' => $address['lastname'], '@street' => $address['street1'], '@city' => $address['city']));
  }

  $types = array('billing');
  if ($txn->shippable) {
    $types[] = 'shipping';
  }

  foreach ($types as $type) {
    $form['ec_customer']['address'][$type] = array(
      '#theme' => 'checkout_address',
      '#prefix' => '<div id="customer-address-'. $type .'" class="customer-address">',
      '#suffix' => '</div>',
    );
    $form['ec_customer']['address'][$type]['#address_type'] = $type;

    if (!empty($txn->{$type .'_address'})) {
      $form['ec_customer']['address'][$type]['display_address'] = array(
        '#value' => theme('formatted_address', $txn->address[$type]),
      );
    }
    else {
      module_load_include('inc', 'ec_store');
      $address = isset($txn->address[$type]) ? $txn->address[$type] : NULL;
      $addr_form = ec_store_address_form($address);
      $form['ec_customer']['address'][$type]+= $addr_form;
      if (ec_customer_get_function($txn->customer->type, 'customer_address_save')) {
        $form['ec_customer']['address'][$type]['save'] = array(
          '#type' => 'checkbox',
          '#title' => t('Save this address for later use.'),
          '#default_value' => isset($txn->address[$type]['save']) ? $txn->address[$type]['save'] : 0,
        );
      }
    }

    if ($type == 'billing' && $txn->shippable) {
      $form['ec_customer']['address'][$type]['use_for_shipping'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use this address as the shipping address'),
        '#default_value' => 1,
        '#id' => 'ec-customer-use-for-shipping'
      );
      drupal_add_js(drupal_get_path('module', 'ec_customer') .'/ec_customer.checkout.js', 'module');
    }

    $form['ec_customer']['address'][$type]['buttons'] = array(
      '#prefix' => '<div class="address-buttons ec-inline-form">',
      '#suffix' => '</div>',
      '#weight' => -1,
    );
    $form['ec_customer']['address'][$type]['buttons']['select_address'] = array(
      '#type' => 'select',
      '#title' => t('Select @type address', array('@type' => $type)),
      '#options' => $options,
      '#access' => count($options) > 1,
    );
    if (isset($txn->{$type .'_address'})) {
      unset($form['ec_customer']['address'][$type]['buttons']['select_address']['#options'][$txn->{$type .'_address'}]);
    }
    $form['ec_customer']['address'][$type]['buttons']['address_submit'] = array(
      '#type' => 'submit',
      '#value' => t('Select address'),
      '#access' => count($options) > 1,
      '#submit' => array('ec_customer_checkout_alter_address'),
      '#ahah' => array(
        'path' => 'checkout/ajax/customer/'. $type,
        'wrapper' => $type .'-address-form',
        'method' => 'replace',
      ),
    );
  }
}

function ec_customer_checkout_alter_address(&$form, &$form_state) {
  $txn =& $form_state['storage']['txn'];
  $type = $form_state['clicked_button']['#parents'][1];
  $txn->{$type .'_address'} = $form_state['values']['address'][$type]['buttons']['select_address'];
  $txn->address['billing'] = $form_state['storage']['addresses'][$txn->{$type .'_address'}];
}

/**
 * AJAX to allow the customer to change address.
 */
function ec_customer_checkout_update_address_js($type = NULL) {
  $cached_form_state = array();
  $files = array();
  // Load the form from the Form API cache.
  if (!($cached_form = form_get_cache($_POST['form_build_id'], $cached_form_state)) || !isset($cached_form_state['storage']['txn']) || empty($type)) {
    form_set_error('form_token', t('Validation error, please try again. If this error persists, please contact the site administrator.'));
    $output = theme('status_messages');
    print drupal_to_js(array('status' => TRUE, 'data' => $output));
    exit();
  }

  $form_state = array('values' => $_POST) + $cached_form_state;
  $txn =& $form_state['storage']['txn'];
  $txn->{$type .'_address'} = $form_state['values']['address'][$type]['buttons']['select_address'];

  $form = array();

  $addresses = $form_state['storage']['addresses'];
  if ($type == 'billing') {
    $ba = $form_state['storage']['txn']->billing_address;
    $address = $addresses[$ba];
  }
  else {
    $sa = $form_state['storage']['txn']->shipping_address;
    $address = $addresses[$sa];
  }

  ec_customer_checkout_form($form, $form_state);
  $cached_form['ec_customer']['address'][$type] = $form['ec_customer']['address'][$type];
  $cached_form_state['storage'] = $form_state['storage'];

  $new_form = array('ec_customer' => array('address' => array('#tree' => TRUE)));
  $new_form['ec_customer']['address'][$type] = $form['ec_customer']['address'][$type];

  $new_form['ec_customer']['address'][$type]['display_address'] = array('#value' => theme('formatted_address', $address));

  $form = $new_form;

  form_set_cache($_POST['form_build_id'], $cached_form, $cached_form_state);

  // Render the form for output.
  $form += array(
    '#post' => $_POST,
    '#programmed' => FALSE,
    '#tree' => FALSE,
    '#parents' => array(),
  );

  unset($form['ec_customer']['address'][$type]['#prefix']);
  unset($form['ec_customer']['address'][$type]['#suffix']);
  if ($type == 'billing') {
    unset($form['ec_customer']['address'][$type]['use_for_shipping']);
  }

  drupal_alter('form', $form, array(), 'ec_customer_checkout_form');
  $form_state = array('submitted' => FALSE);
  $form = form_builder('ec_product_form', $form, $form_state);
  $output = theme('status_messages') . drupal_render($form);

  // We send the updated file attachments form.
  // Don't call drupal_json(). ahah.js uses an iframe and
  // the header output by drupal_json() causes problems in some browsers.
  print drupal_to_js(array('status' => TRUE, 'data' => $output));
  exit;
}

/**
 * Check, prepare and sanitize addresses on a transaction.
 *
 * @param $txn
 *  An ecommerce transaction or any similar object that stores address
 *  data on an address attribute.
 *
 * @return
 *  Returns FALSE if addresses could not be resolved. This will mean
 *  that the calling code needs
 *
 */
function ec_customer_prepare_addresses(&$txn, $addresses = NULL) {
  if ($addresses) {
    // The presence of new address data means we are updating the transaction.
    $txn->address = array();
  }

  // Assume that any $txn->address is valid.
  if (!empty($txn->address)) {
    return TRUE;
  }

  // If we lack addresses, try to gather them or return FALSE
  if (!$addresses && !isset($txn->address)) {
    if (!ec_customer_get_attr($txn->customer->type, 'store_addresses', TRUE)) {
      // We don't an address provider!
      return FALSE;
    }
    else {
      $addresses = ec_customer_get_addresses($txn->customer);
      if (!$addresses) {
        return FALSE;
      }
    }
  }

  // Manage the addresses returned from the address provider.
  if ($txn->shippable) {
    $txn->address['billing'] = $txn->address['shipping'] = reset($addresses);
    if (isset($txn->address['billing']->aid)) {
      $txn->billing_address = $txn->shipping_address = $txn->address['billing']['aid'];
    }
  }
  elseif (variable_get('ec_customer_billing_address', 1)) {
    if (!empty($addresses)) {
      $txn->address['billing'] = reset($addresses);
      if (isset($txn->address['billing']['aid'])) {
        $txn->billing_address = $txn->address['billing']['aid'];
      }
    }
    else {
      $txn->billing_address = NULL;
    }
  }
  else {
    $txn->address = array();
  }
}

/**
 * The form to set the shipping and billing address in the checkout procedure.
 */
function ec_customer_address_form($txn) {

  // Grab the user's addressbook
  $address = ec_customer_get_addresses($txn->customer);

  if (empty($address) && ec_customer_get_attr($txn->customer->type, 'store_addresses', TRUE)) {
    $goto = ec_customer_links($txn->customer, 'add_address', drupal_get_destination());
    call_user_func_array('drupal_goto', $goto);
  }

  // TODO: this function seems broken from here.

  // Build address options
  $options = array();
  foreach ($address as $value) {
    $options[$value->aid] = t('@name, @street in @city',
      array(
        '@name' => isset($value->fullname) ? $value->fullname : "{$value->firstname} {$value->lastname}",
        '@street' => $value->street1,
        '@city' => $value->city,
      ));
  }
  if (!empty($txn->shippable)) {
    $form['shipping_address'] = array(
      '#type'           => 'radios',
      '#title'          => t('Shipping to'),
      '#default_value'  => $txn->shipping_address,
      '#options'        => $options,
      '#description'    => t('Please choose where you would like the items to be delivered.') .
        (ec_customer_get_attr($txn->customer, 'add_address', TRUE) ?
        ' '. t('You can also <a href="!add_address">add a new address</a>.', array(
        '!add_address' => call_user_func_array('url',
        ec_customer_links($txn->customer, 'add_address', drupal_get_destination())))) : '')
    );
  }
  $form['billing_address'] = array(
    '#type'           => 'radios',
    '#title'          => t('Billing to'),
    '#default_value'  => $txn->billing_address,
    '#options'        => $options,
    '#description'    => t('Please choose where you would like the bill to be delivered.') .
      (ec_customer_get_attr($txn->customer, 'add_address', TRUE) ?
      ' '. t('You can also <a href="!add_address">add a new address</a>.',
      array('!add_address' => call_user_func_array('url',
      ec_customer_links($txn->customer, 'add_address', drupal_get_destination())))) : '')
  );
  #$form['address']['#tree'] = TRUE;
  return $form;
}

/**
 * Implementation of hook_checkout_update().
 */
function ec_customer_checkout_update(&$form, &$form_state) {
  $txn =& $form_state['storage']['txn'];

  if (!isset($txn->billing_address) && $form_state['values']['address']['billing']) {
    $txn->address['billing'] = $form_state['values']['address']['billing'];
  }

  if ($txn->shippable) {
    if (isset($form_state['values']['address']['billing']['use_for_shipping'])) {
      $txn->address['billing']['use_for_shipping'] = $form_state['values']['address']['billing']['use_for_shipping'];
    }
    if ($txn->address['billing']['use_for_shipping']) {
      $txn->address['shipping'] = $txn->address['billing'];
      unset($txn->address['shipping']['save']);
    }
    else if (!isset($txn->shipping_address) && $form_state['values']['address']['shipping']) {
      $txn->address['shipping'] = $form_state['values']['address']['shipping'];
    }
  }
}

/**
 * Implementation of hook_checkout_post_submit().
 */
function ec_customer_checkout_post_submit($txn, &$form_state) {
  if ($function = ec_customer_get_function($txn->customer->type, 'customer_address_save')) {
    foreach ($txn->address as $type => $address) {
      if (isset($form_state['values']['address'][$address['type']]['save']) && $form_state['values']['address'][$address['type']]['save']) {
        $function($txn->customer, $txn->address[$type]);
      }
    }
  }
}

/**
 * Implementation of hook_checkout_init().
 */
function ec_customer_email_checkout_form(&$form, &$form_state) {
  $txn =& $form_state['storage']['txn'];
  $form['ec_customer_email'] = array(
    '#theme' => 'ec_mail_checkout_form',
    '#txn' => &$form_state['storage']['txn'],
  );
  if ($txn->customer->type == 'anonymous') {
    $form['ec_customer_email']['mail'] = array(
      '#type' => 'textfield',
      '#title' => t('Email'),
      '#default_value' => $txn->mail,
      '#validate' => array('ec_anon_validate_mail' => array()),
      '#required' => TRUE,
    );
  }
  else {
    $form['ec_customer_email']['mail'] = array(
      '#value' => !empty($txn->mail) ? $txn->mail : ec_customer_get_email($txn->ecid),
    );
    $form['ec_customer_email']['mail']['#access'] = variable_get('ec__display_email', 1);
  }
}

/**
 * Implementation of hook_checkout_submit().
 */
function ec_customer_email_checkout_submit(&$form, &$form_state) {
  $txn =& $form_state['storage']['txn'];
  if (!empty($form_state['values']['mail'])) {
    $txn->mail = $form_state['values']['mail'];
  }
}

