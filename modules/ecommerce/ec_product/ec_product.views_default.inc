<?php
// $Id: ec_product.views_default.inc,v 1.1.2.3 2009/01/09 03:04:25 gordon Exp $
/**
 * @file
 * Provide default product views
 */

/**
 * Implementation of hook_views_default_views().
 */
function ec_product_views_default_views() {
  $views = array();

$view = new view;
$view->name = 'ec_product';
$view->description = 'List of products in the store';
$view->tag = 'ecommerce product';
$view->view_php = '';
$view->base_table = 'node';
$view->is_cacheable = FALSE;
$view->api_version = 2;
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
$handler = $view->new_display('default', 'Defaults', 'default');
$handler->override_option('fields', array(
  'title' => array(
    'label' => '',
    'link_to_node' => 1,
    'exclude' => 0,
    'id' => 'title',
    'table' => 'node',
    'field' => 'title',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
  'teaser' => array(
    'label' => '',
    'exclude' => 0,
    'id' => 'teaser',
    'table' => 'node_revisions',
    'field' => 'teaser',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
  'price' => array(
    'label' => '',
    'exclude' => 0,
    'id' => 'price',
    'table' => 'ec_product',
    'field' => 'price',
    'relationship' => 'none',
    'override' => array(
      'button' => 'Override',
    ),
  ),
  'checkout_links' => array(
    'label' => '',
    'exclude' => 0,
    'id' => 'checkout_links',
    'table' => 'ec_product',
    'field' => 'checkout_links',
    'relationship' => 'none',
  ),
));
$handler->override_option('filters', array(
  'status' => array(
    'operator' => '=',
    'value' => '1',
    'group' => '0',
    'exposed' => FALSE,
    'expose' => array(
      'operator' => FALSE,
      'label' => '',
    ),
    'id' => 'status',
    'table' => 'node',
    'field' => 'status',
    'relationship' => 'none',
  ),
  'is_product' => array(
    'operator' => '=',
    'value' => '1',
    'group' => '0',
    'exposed' => FALSE,
    'expose' => array(
      'operator' => FALSE,
      'label' => '',
    ),
    'label' => 'Is product',
    'type' => 'yes-no',
    'not' => 0,
    'exclude' => 0,
    'id' => 'is_product',
    'table' => 'ec_product',
    'field' => 'is_product',
    'override' => array(
      'button' => 'Override',
    ),
    'relationship' => 'none',
  ),
));
$handler->override_option('access', array(
  'type' => 'none',
));
$handler->override_option('title', 'Products');
$handler->override_option('style_plugin', 'grid');
$handler->override_option('style_options', array(
  'grouping' => '',
  'columns' => '3',
  'alignment' => 'horizontal',
));
$handler = $view->new_display('page', 'Page', 'page_1');
$handler->override_option('path', 'products');
$handler->override_option('menu', array(
  'type' => 'normal',
  'title' => 'Products',
  'weight' => '0',
  'name' => 'navigation',
));
$handler->override_option('tab_options', array(
  'type' => 'none',
  'title' => '',
  'weight' => 0,
));


  $views[$view->name] = $view;

  return $views;
}

