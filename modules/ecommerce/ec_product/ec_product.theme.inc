<?php
// $Id: ec_product.theme.inc,v 1.1.2.3 2009/02/17 21:49:56 gordon Exp $

/**
 * @file
 * ec product theme's
 */

/**
 * @ingroup themeable
 */
function theme_invoice_form_product(&$form) {
  $output .= drupal_render($form);
  return $output;
}

/**
 * @ingroup themeable
 */
function theme_price($element) {
  return theme('textfield', $element);
}

/**
 * Display the product price
 *
 * @param $node
 *   Object. The node
 * @ingroup themeable
 *
 * TODO: this function should not be adjusting price since it's a theme function.
 *       move the adjustments to the calling location.
 */
function theme_ec_product_price($node) {
  // Add the CSS
  drupal_add_css(drupal_get_path('module', 'product') .'/ec_product.css');

  return '<div class="price">'.
    format_currency(ec_product_get_final_price($node, 'product')) .'</div>';
}

/**
 * @ingroup themeable
 */
function theme_ec_product_autocomplete($product) {
  $output .= "<b>{$product->nid}</b> - ". check_plain($product->title) .
    (isset($product->sku) ? '[<i>'. check_plain($product->sku) .'</i>]' : '');
  return $output;
}

/**
 * @ingroup themeable
 */
function theme_ec_product_admin_ptypes_feature_list($form) {
  $output = '';

  if (isset($form['features'])) {
    $heading = array(t('Name'), t('Description'), t('Weight'), t('Operations'));
    foreach (element_children($form['features']) as $element) {
      $rows[] = array(
        array('data' => drupal_render($form['features'][$element]['name'])),
        array('data' => drupal_render($form['features'][$element]['description'])),
        array('data' => drupal_render($form['features'][$element]['weight'])),
        array('data' => isset($form['features'][$element]['delete']) ?
          drupal_render($form['features'][$element]['delete']) : ''),
      );
    }

    $output .= theme('table', $heading, $rows);
  }

  return $output . drupal_render($form);
}

/**
 * @ingroup themeable
 */
function theme_ec_product_edit_form(&$form) {
  $output .= drupal_render($form['products']);
  $output .= drupal_render($form);
  return $output;
}

/**
 * @ingroup themeable
 */
function theme_ec_product_edit_form_product(&$form) {
  $children = element_children($form);
  if (!empty($children)) {
    $extra = FALSE;

    foreach ($children as $nid) {
      if (!empty($form[$nid]['data'])) {
        $extra = TRUE;
        break;
      }
    }

    foreach ($children as $nid) {
      $total+= $form[$nid]['#total'];
      $desc = drupal_render($form[$nid]['title']) .'<br />';
      if (!empty($form[$nid]['recurring'])) {
        $desc .= '<div class="recurring-details">'. drupal_render($form[$nid]['recurring']) .'</div>';
      }
      if (!empty($form[$nid]['availability'])) {
        $desc .= drupal_render($form[$nid]['availability']);
      }
      $desc .= '<p>'. format_currency($form[$nid]['#total']) .'</p>';
      $row = array(
        array('data' => $desc),
        array('data' => drupal_render($form[$nid]['qty'])),
      );
      if (!empty($extra)) {
        if (!empty($form[$nid]['node'])) {
          $row[] = array('data' => drupal_render($form[$nid]['node']));
          $extra = TRUE;
        }
        else {
          $row[] = '';
        }
      }
      $row[] = array('data' => drupal_render($form[$nid]['delete']), 'align' => 'center');
      $rows[] = $row;
    }
    $header = array(
      array('data' => t('Items')),
      array('data' => t('Qty')),
    );
    if (!empty($extra)) {
      $header[] = array('data' => '');
    }
    $header[] = array('data' => t('Delete'));
    $output .= theme('table', $header, $rows);
  }
  else {
    $output .= '<p>'. t('No Products Currently on this Invoice') .'</p>';
  }
  $output .= drupal_render($form);
  return $output;
}

/**
 * Display all products in a table format.
 * @ingroup themable
 */
function theme_ec_product_overview($rows, $header) {
  if (!empty($rows)) {
    $output = theme('table', $header, $rows);
  }
  else {
    $output = '<p>'. t('There are no products to view.') .'</p>';
  }
  return $output;
}
