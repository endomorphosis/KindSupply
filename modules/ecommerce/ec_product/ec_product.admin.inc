<?php
// $Id: ec_product.admin.inc,v 1.10.2.12 2009/03/10 21:25:16 darrenoh Exp $

/**
 * @file
 * This file contains all the administration code for configuring
 * e-Commerce products - features, attributes, types.
 * @author e-Commerce Dev Team
 */

/**
 * Elements common to every single product: price, SKU and
 * the option to show a "Add to cart" as a link or a button.
 */
function ec_product_form_base_elements(&$node) {
  $form['ptype'] = array(
    '#type' => 'value',
    '#value' => $node->ptype
  );
  $form['price'] = array(
    '#type' => 'price',
    '#title' => t('Price'),
    '#size' => 25,
    '#maxlength' => 50,
    '#required' => TRUE,
    '#default_value' => format_currency(!empty($node->price) ? $node->price : 0),
    '#description' => t('How much does this product retail for? Note: This price may be different from the selling price due to price adjustments elsewhere.'),
    '#weight' => -20,
  );
  $form['sku'] = array(
    '#type' => 'textfield',
    '#title' => t('SKU'),
    '#size' => 25,
    '#maxlength' => 50,
    '#default_value' => !empty($node->sku) ? $node->sku : '',
    '#description' => t('If you have an unique identifier for this product from another system or database, enter that here. This is optional, as system IDs are automatically created for each product.'),
    '#weight' => -10,
  );

  return $form;
}

/**
 * Generates product type options fieldset.
 * Calling function (product_form_alter) has already checked that the node is a product.
 */
function _ec_product_alter_node_form(&$node) {
  drupal_add_css(drupal_get_path('module', 'product') .'/ec_product.css');
  // Product type collapsed fieldset
  $form = array(
    '#type' => 'fieldset',
    '#title' => t('Product'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#prefix' => '<div id="ec-product-wrapper">',
    '#suffix' => '</div>',
  );
  // Choose a product type.
  $ptypes[FALSE] = array('name' => t('Not a product'), 'help' => t('This item not for sale.'));
  $types = variable_get('ec_product_ptypes_'. $node->type, NULL);
  if (empty($types)) {
    $types = array_keys(ec_product_ptypes_get('names'));
  }

  // Populate product type properties.
  foreach ($types as $key) {
    $info = ec_product_ptypes_get('type', $key);
    if (ec_product_ptypes_access($info->name)) {
      $ptypes[$key] = array(
        'name' => $info->name,
        'help' => $info->description,
      );
    }
  }
  // Display each radio option, with descriptive text.
  foreach ($ptypes as $key => $data) {
    $form['ptype'][$key] = array(
      '#type' => 'radio',
      '#title' => $data['name'],
      '#return_value' => $key,
      '#description' => $data['help'],
      '#parents' => array('ptype'),
    );
  }
  $form['product_add'] = array(
    '#type' => 'submit',
    '#value' => t('Add to store'),
    '#submit' => array('node_form_submit_build_node'),
    '#ahah' => array(
      'path' => 'ec_store/convertnode/js',
      'wrapper' => 'ec-product-wrapper',
    ),
  );
  return $form;
}

/**
 * Generates product form fields to be added into the node form.
 * Called from ec_product_form_alter().
 *
 * @param $form_id, $form_values, as passed to hook_form_alter.
 * @ingroup form
 */
function _ec_product_alter_product_form(&$form, &$node_form) {
  $product_type = ec_product_ptypes_get('type', $node_form['#node']);
  // Get base form elements.
  $pform = array(
    '#type' => 'fieldset',
    '#title' => t('Product'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Product details') .' ('. $product_type->name .')',
    '#prefix' => '<div id="ec-product-wrapper">',
    '#suffix' => '</div>',
  );
  $pform = array_merge($pform, ec_product_form_base_elements($node_form['#node']));
  $pform['ptype']['#type'] = 'hidden';

  // If this type is from a specific module, get its bits.
  // TODO: alter receiving modules so that they listen for hook_product_form_alter().
  $pform = array_merge($pform, (array) ec_product_invoke_productapi($node_form['#node'], 'form'));

  // Allow other modules to alter the product form ($pform).
  // Preferable over them implementing hook_form_alter directly.
  foreach ($product_type->features as $feature) {
    if (isset($feature->fileinc)) {
      require_once $feature->fileinc;
    }
  }
  foreach (module_implements('product_form_alter') as $module) {
    $func = $module .'_product_form_alter';
    $func($pform, $node_form);
  }

  // Add remove option if applicable.
  if (variable_get('ec_product_convert_'. $node_form['#node']->type, EC_PRODUCT_CONVERT_NEVER) != EC_PRODUCT_CONVERT_ALWAYS) {
    $pform['product_collapsed']['product_remove'] = array(
      '#type' => 'checkbox',
      '#title' => t('Remove from store'),
      '#description' => t('Check here to delete product information. Takes effect when changes are submitted.')
    );
  }
  return $pform;
}

/**
 * Additional settings that are added to the node type form at admin/content/types/*
 *
 * @param $form_id, $form_values, as passed to hook_form_alter.
 * @ingroup form
 */
function _ec_product_alter_node_type_form(&$form, &$form_state, $form_id) {
  $form['product'] = array(
    '#type' => 'fieldset',
    '#title' => t('Product'),
    '#collapsible' => TRUE,
    '#weight' => 1,
  );
  $type = $form['old_type']['#value'];
  $form['product']['ec_product_convert'] = array(
    '#type' => 'select',
    '#title' => t('Node type is always a product'),
    '#default_value' => ec_product_can_be_converted($type),
    '#options' => array(EC_PRODUCT_CONVERT_NEVER => t('Never'), EC_PRODUCT_CONVERT_ALWAYS => t('Always'), EC_PRODUCT_CONVERT_OPTIONAL => t('Optional')),
    '#description' => t('Select the setting which will determine how the product is going to interact with the node'),
  );
  $ptypes = ec_product_ptypes_get();
  $options = array();
  foreach ($ptypes as $ptype => $info) {
    drupal_add_css(drupal_get_path('module', 'product') .'/ec_product.css');
    $options[$ptype] = t('!name<dt>!description</dt>',
      array('!name' => $info->name, '!description' => $info->description));
  }
  $ptypes = variable_get('ec_product_ptypes_'. $type, array_keys(ec_product_ptypes_get('names')));
  $form['product']['ec_product_ptypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Product types'),
    '#default_value' => $ptypes,
    '#options' => $options,
    '#description' => t('Indicate the products types that wil be available on the current type of node.'),
  );
  $form['delete']['#weight'] = 11;
  $form['submit']['#weight'] = 10;
}

/**
 * Update the node form to include a product form.
 */
function ec_project_convert_node_js() {
  $cached_form_state = array();
  $files = array();

  // Load the form from the Form API cache.
  if (!($cached_form = form_get_cache($_POST['form_build_id'], $cached_form_state)) || !isset($cached_form['#node']) || !isset($cached_form['attachments'])) {
    form_set_error('form_token', t('Validation error, please try again. If this error persists, please contact the site administrator.'));
    $output = theme('status_messages');
    print drupal_to_js(array('status' => TRUE, 'data' => $output));
    exit();
  }

  $form_state = array('values' => $_POST);

  $cached_form['#node']->ptype = $form_state['values']['ptype'];
  $node = $cached_form['#node'];
  $old_form = array();

  $form = _ec_product_alter_product_form($old_form, $cached_form);
  $cached_form['product'] = $form;

  form_set_cache($_POST['form_build_id'], $cached_form, $cached_form_state);

  // Render the form for output.
  $form += array(
    '#post' => $_POST,
    '#programmed' => FALSE,
    '#tree' => FALSE,
    '#parents' => array(),
  );
  drupal_alter('form', $form, array(), 'ec_product_form');
  $form_state = array('submitted' => FALSE);
  $form = form_builder('ec_product_form', $form, $form_state);
  $output = theme('status_messages') . drupal_render($form);

  // We send the updated file attachments form.
  // Don't call drupal_json(). ahah.js uses an iframe and
  // the header output by drupal_json() causes problems in some browsers.
  print drupal_to_js(array('status' => TRUE, 'data' => $output));
  exit;
}

/**
 * List all product types
 * Path: admin/ecsettings/ptypes
 */
function ec_product_admin_ptypes() {
  $ptypes = ec_product_ptypes_get();
  uasort($ptypes, create_function('$a, $b', 'if ($a->name == $b->name) { return 0; } else { return strcmp($a->name, $b->name); }'));
  $output = '';
  if (!empty($ptypes)) {
    $head = array(t('Name'), t('Type'), t('Description'), array(
      'data' => t('Operations'), 'colspan' => 2));
    foreach ($ptypes as $ptype => $item) {
      $rows[] = array(
        array('data' => l($item->name, "admin/ecsettings/ptypes/{$ptype}")),
        array('data' => check_plain($ptype)),
        array('data' => check_plain($item->description)),
        array('data' => l(t('edit'), "admin/ecsettings/ptypes/{$ptype}")),
        array('data' => !isset($item->internal) ? l(t('delete'),
          "admin/ecsettings/ptypes/{$ptype}/delete") : ''),
      );
    }
    $output .= theme('table', $head, $rows);
  }
  else {
    drupal_set_message(t('No product types defined.'));
  }
  return $output;
}

/**
 * Add or Edit product type form.
 * Path: admin/ecsettings/ptypes/add, admin/ecsettings/ptypes/*
 */
function ec_product_admin_ptypes_form($form_state, $ptype = NULL) {
  if (!empty($ptype) && (!$info = ec_product_ptypes_get('type', $ptype))) {
    drupal_not_found();
  }
  drupal_add_css(drupal_get_path('module', 'ec_product') .'/ec_product.css');
  $form = array();
  if (empty($info)) {
    $info = new stdClass;
    $info->name = $info->ptype = $info->description = '';
  }
  else {
    $form['old_ptype'] = array(
      '#type' => 'value',
      '#value' => $info->ptype,
    );
  }
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $info->name,
    '#description' => t('The human readable name of this product type.'),
    '#required' => TRUE,
  );
  $form['ptype'] = array(
    '#type' => 'textfield',
    '#title' => t('Type'),
    '#default_value' => $info->ptype,
    '#description' => t('The machine-readable name of this product type. This is used by the system internally to refer to this product type. The Product type may not include space or -'),
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $info->description,
    '#description' => t('A brief description of what this product type is.'),
    '#required' => TRUE,
  );
  $form['info'] = array(
    '#type' => 'value',
    '#value' => $info,
  );

  $defaults = array();
  $options = array();
  foreach (node_get_types() as $type => $info) {
    $options[$type] = t('@name<dt>!description<dt>', array('@name' => $info->name, '!description' => $info->description));
    $product = variable_get('ec_product_ptypes_'. $type, array());
    if (in_array($ptype, $product)) {
      $defaults[] = $type;
    }
  }

  if (!empty($options)) {
    $form['content_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Content Types'),
      '#description' => t('Select content types which this this product type can be used on.'),
      '#default_value' => $defaults,
      '#options' => $options,
    );
  }

  $form['submit'] = array();
  $form['submit']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Submit product type'),
  );
  if (isset($info->internal) && empty($info->internal)) {
    $form['submit']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset product type'),
    );
  }
  elseif (!empty($info->ptype) && !db_result(db_query_range("SELECT COUNT(*) FROM {ec_product}
      WHERE ptype = '%s'", $info->ptype, 0, 1))) {
    $form['submit']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete product type'),
    );
  }

  return $form;
}

/**
 * Validate product_admin_ptypes_form()
 * @ingroup form
 */
function ec_product_admin_ptypes_form_validate(&$formd, &$form_state) {
  if (!empty($form_state['values']['old_ptype']) && $form_state['values']['ptype'] != $form_state['values']['old_ptype'] &&
      ec_product_ptypes_get('type', $form_state['values']['ptype'])) {
    form_set_error('ptype', t('Product type %ptype already exists', array('%ptype' => $form_state['values']['ptype'])));
  }
  else if (preg_match('/[- ]/i', $form_state['values']['ptype'])) {
    form_set_error('ptype', t('Product type may not include any - or spaces'));
  }
}

/**
 * Submit product_admin_ptypes_form()
 * @ingroup form
 */
function ec_product_admin_ptypes_form_submit(&$form, &$form_state) {
  $info = $form_state['values']['info'];
  $info->name = $form_state['values']['name'];
  $info->ptype = $form_state['values']['ptype'];
  $info->old_ptype = isset($form_state['values']['old_ptype']) ? $form_state['values']['old_ptype'] : NULL;
  $info->description = $form_state['values']['description'];

  if ($form_state['values']['op'] == t('Reset product type')) {
    ec_product_ptypes_delete($info->ptype);
    drupal_set_message(t('Product type %ptype has been reset',
      array('%ptype' => $info->ptype)));
    $form_state['redirect'] = 'admin/ecsettings/ptypes/'. $info->ptype;
  }
  elseif ($form_state['values']['op'] == t('Delete product type')) {
    $form_state['redirect'] = 'admin/ecsettings/ptypes/'. $info->ptype .'/delete';
    return;
  }
  elseif ($form_state['values']['op'] == t('Submit product type')) {
    ec_product_ptypes_save($info);

    foreach (node_get_types() as $type => $info) {
      $ptypes = variable_get('ec_product_ptypes_'. $type, array());

      if ($form_state['values']['old_ptype'] != $form_state['values']['ptype'] && isset($form_state['values']['content_types'][$type]) && $form_state['values']['content_types'][$type] && ($pos = array_search($form_state['values']['old_ptype'], $ptypes)) !== FALSE) {
        unset($ptypes[$pos]);
        $ptypes[] = $form_state['values']['ptype'];
      }
      elseif (isset($form_state['values']['content_types'][$type]) && $form_state['values']['content_types'][$type]) {
        $ptypes[] = $form_state['values']['ptype'];
      }
      elseif (isset($form_state['values']['content_types'][$type]) && !$form_state['values']['content_types'][$type] && ($pos = array_search($form_state['values']['old_ptype'], $ptypes)) !== FALSE) {
        unset($ptypes[$pos]);
      }
      variable_set('ec_product_ptypes_'. $type, $ptypes);
    }
  }
  $form_state['redirect'] = 'admin/ecsettings/ptypes';
}

/**
 * Implementation of hook_delete().
 */
function _ec_product_delete($node, $revision_only = FALSE) {
  if (!empty($revision_only)) {
    db_query('DELETE FROM {ec_product} WHERE nid = %d AND vid = %d', $node->nid, $node->vid);
  }
  else {
    db_query('DELETE FROM {ec_product} WHERE nid = %d', $node->nid);
  }
  module_invoke('cart', 'productapi', $node, 'delete');
  ec_product_invoke_productapi($node, 'delete', $revision_only);
  ec_product_invoke_feature_all($node, 'delete', $revision_only);
  drupal_set_message(t('Product deleted'));
}

/**
 * @param $form_id
 * @param $form_values
 * @ingroup form
 */
function _ec_product_edit_form(&$invoice) {
  $form['#theme'] = 'ec_product_edit_form';
  $form['products'] = array(
    '#type' => 'fieldset',
    '#title' => t('Products'),
    '#tree' => TRUE,
    '#theme' => 'ec_product_edit_form_product',
  );
  foreach ((array)$invoice->items as $key => $item) {
    $node = node_load($item->nid);
    $form['products'][$node->nid]['#node'] = $item;
    $form['products'][$node->nid]['title'] = array(
      '#value' => l($item->title, 'node/'. (isset($node->pparent) ? $node->pparent : $node->nid)),
    );
    $price = store_adjust_misc($invoice, $item);
    $form['products'][$node->nid]['#total'] = ec_product_has_quantity($node) ?
      $price * $item->qty : $price;
    if (!empty($node->is_recurring)) {
      $form['products'][$node->nid]['recurring'] = array(
        '#value' => ec_product_recurring_nice_string($node)
      );
    }
    if ($node->ptype == 'tangible' && $node->availability != NULL && $node->availability != 1) {
      $form['products'][$node->nid]['availability'] = array(
        '#value' => availability_get_message($node->availability)
      );
    }
    $form['products'][$node->nid]['qty'] = array(
      '#type' => ec_product_has_quantity($node) ? 'textfield' : 'value',
      '#default_value' => $item->qty,
      '#size' => 2,
      '#maxlength' => 2
    );
    $form['products'][$node->nid]['delete'] = array(
      '#type' => 'checkbox',
    );
    if ($extra = ec_product_invoke_productapi($item, 'cart_form', $invoice)) {
      $form['#product_cart_form'] = TRUE;
      $form['products'][$node->nid]['data'] = array();
    }
  }
  $form['add'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add Products'),
    '#collapsible' => TRUE,
    '#collapsed' => count($invoice->items) ? TRUE : FALSE,
  );
  $form['add']['new_products'] = array(
    '#type' => 'textfield',
    '#title' => t('Product Ids'),
    '#maxlength' => 60,
    '#autocomplete_path' => 'product/autocomplete/all',
    '#description' => t('Enter the id\'s of the products that you which to add to the invoice. Here is a <a href="!product_quicklist" onclick="window.open(this.href, \'!product_quicklist\', \'width=480,height=480,scrollbars=yes,status=yes\'); return FALSE">list of all products</a>.', array('!product_quicklist' => url('admin/store/products/quicklist'))),
  );
  $form['submit']['invoiceop'] = array(
    '#type' => 'submit',
    '#value' => t('Update Products'),
    '#attributes' => array('id' => 'invoiceop'),
  );
  $form['submit']['continue'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
  );
  return $form;
}

/**
 * List all features from a given product type.
 * Path: admin/ecsettings/ptypes/.../features
 *
 * @param $info
 *   Object, the product type
 */
function ec_product_admin_ptypes_feature_list($form_state, $ptype = NULL) {
  if (!$info = ec_product_ptypes_get('type', $ptype)) {
    drupal_not_found();
  }
  $form = array();
  if (!empty($info->features)) {
    $form['features'] = array(
      '#tree' => TRUE,
    );
    foreach ($info->features as $ftype => $feature) {
      $form['features'][$ftype]['ptype'] = array(
        '#type' => 'value',
        '#value' => $info->ptype,
      );
      $form['features'][$ftype]['ftype'] = array(
        '#type' => 'value',
        '#value' => $feature->ftype,
      );
      $form['features'][$ftype]['name'] = array(
        '#value' => $feature->name,
      );
      $form['features'][$ftype]['description'] = array(
        '#value' => $feature->description,
      );
      $form['features'][$ftype]['weight'] = array(
        '#type' => 'weight',
        '#default_value' => $feature->weight,
        '#disabled' => isset($feature->allow_edit) ? !$feature->allow_edit : FALSE,
      );
      if (!isset($feature->allow_disable) || $feature->allow_disable) {
        $form['features'][$ftype]['delete'] = array(
          '#value' => l(t('delete'), 'admin/ecsettings/ptypes/'. $info->ptype .
          '/features/'. $feature->ftype .'/delete'),
        );
      }
    }
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Update features',
    );
  }
  else {
    drupal_set_message(t('No features have been added to this product'));
  }
  return $form;
}

/**
 * Submit for ec_product_admin_ptypes_feature_list()
 * @ingroup form
 */
function ec_product_admin_ptypes_feature_list_submit(&$form, &$form_state) {
  foreach ($form_state['values']['features'] as $feature) {
    ec_product_feature_update((object)$feature);
  }
  drupal_set_message(t('Features have been updated'));
}

/**
 * Link product types to features.
 * @ingroup form
 * Path: 'admin/ecsettings/ptypes/.../add_feature
 */
function ec_product_admin_ptypes_feature_form($form_state, $ptype) {
  if (!$info = ec_product_ptypes_get('type', $ptype)) {
    drupal_not_found();
  }
  $options = $form = array();
  $features = ec_product_feature_get();
  foreach ($features as $ftype => $feature) {
    if (!isset($info->features[$ftype])) {
      $options[$ftype] = t('@name<dt>@description</dt>',
        array('@name' => $feature->name, '@description' => $feature->description));
    }
  }
  if (!empty($options)) {
    $form['info'] = array(
      '#type' => 'value',
      '#value' => $info,
    );
    $form['ftype'] = array(
      '#type' => 'radios',
      '#title' => t('Select a product feature to be added'),
      '#options' => $options,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add product feature'),
    );
  }
  else {
    drupal_set_message(t('No more features can be added to @name',
      array('@name' => $info->name)));
    drupal_goto('admin/ecsettings/ptypes/'. $info->ptype .'/features');
  }
  return $form;
}

/**
 * Submit for ec_product_admin_ptypes_feature_form()
 * @ingroup form
 */
function ec_product_admin_ptypes_feature_form_submit(&$form, &$form_state) {
  ec_product_feature_enable($form_state['values']['info'], $form_state['values']['ftype']);
  $form_state['redirect'] = 'admin/ecsettings/ptypes/'. $form_state['values']['info']->ptype .'/features';
}

/**
 * Overview of settings of a ptype.
 * @ingroup $form
 * Path: admin/ecsettings/ptypes/settings
 */
function ec_product_admin_ptypes_settings() {
  $form = array();

  $form['ec_product_convert'] = array(
    '#type' => 'select',
    '#title' => t('New node types product conversion setting'),
    '#default_value' => variable_get('ec_product_convert', 0),
    '#options' => array(
      0 => t('Never'),
      1 => t('Always'),
      2 => t('Optional'),
    ),
    '#description' => t('This setting will be applied to all new node types when they are created'),
  );

  $form['ec_product_types'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add new product types to nodes automatically'),
    '#default_value' => variable_get('ec_product_types', 1),
    '#description' => t('Set the default action for all new product types'),
  );

  $form['ec_product_cart_addition_by_link'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Users adds product to cart using link.'),
    '#default_value'  => variable_get('ec_product_cart_addition_by_link', 1),
    '#description'    => t('If this field is unchecked a form will be placed at the bottom of the product view which will allow the user select the quantity and other information.'),
  );

  $form['ec_product_cart_on_teaser'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display cart on node teaser display'),
    '#default_value' => variable_get('ec_product_cart_on_teaser', 1),
    '#description' => t('Show the cart links/form on node teasers.'),
  );

  return system_settings_form($form);
}

/**
 * Delete a product type.
 * @ingroup $form
 * Path: admin/ecsettings/ptypes/.../delete
 */
function ec_product_admin_ptypes_delete($form_state, $ptype) {
  if (!$info = ec_product_ptypes_get('type', $ptype)) {
    drupal_not_found();
  }
  $form = array();

  if (db_result(db_query_range("SELECT COUNT(*) FROM {ec_product} WHERE ptype = '%s'", $info->ptype, 0, 1))) {
    drupal_set_message(t('You cannot delete product type %name as products still exist', array('%name' => $info->name)), 'error');
    drupal_goto('admin/ecsettings/ptypes');
  }

  $form['info'] = array(
    '#type' => 'value',
    '#value' => $info,
  );

  return confirm_form($form, t('Are you sure you want to delete product type %name?', array('%name' => $info->name)), 'admin/ecsettings/ptypes', t('This action cannot be undone'), t('Delete product type'), t('Cancel'));
}

/**
 * Submit product_admin_ptypes_delete()
 * @ingroup form
 */
function ec_product_admin_ptypes_delete_submit(&$form, &$form_state) {
  $info = $form_state['values']['info'];
  ec_product_ptypes_delete($info->ptype);
  drupal_set_message(t('Product type %name has been deleted', array('%name' => $info->name)));
  $form_state['redirect'] = 'admin/ecsettings/ptypes';
}

/**
 * Delete a product type
 */
function ec_product_ptypes_delete($ptype) {
  $info = ec_product_ptypes_get('type', $ptype, TRUE);
  db_query("DELETE FROM {ec_product_ptypes} WHERE ptype = '%s'", $ptype);
  module_invoke_all('product_types', $info, 'delete');
  menu_rebuild();
}

/**
 * Implementation of hook_product_types().
 * Which is also in the admin file.
 */
function ec_product_product_types($info, $op) {
  switch ($op) {
    case 'update':
      if (isset($info->old_ptype) && variable_get('ec_product_ptypes', 1)) {
        foreach (node_get_types('names') as $type => $description) {
          $ptypes = variable_get('ec_product_ptypes_'. $type, array());
          if (!in_array($info->ptype, $ptypes)) {
            $ptypes[] = $info->ptype;
          }
        }
      }
      break;

    case 'delete':
      foreach ($info->features as $ftype => $feature) {
        ec_product_feature_disable($info, $ftype);
      }
  }
}


/**
 * Confirm if the product feature should be deleted
 *
 * @param $info
 * @param $features
 *   Object, the feature
 * @ingroup form
 */
function ec_product_admin_ptypes_feature_delete($form_state, $ptype, $ftype) {
  if (!$info = ec_product_ptypes_get('type', $ptype)) {
    drupal_not_found();
  }
  if (!$feature = ec_product_feature_get('type', $ftype)) {
    drupal_not_found();
  }
  $form = array();

  $form['info'] = array(
    '#type' => 'value',
    '#value' => $info,
  );
  $form['feature'] = array(
    '#type' => 'value',
    '#value' => $feature,
  );

  return confirm_form($form, t('Are you sure you want to delete feature %feature_name from product type %name?',
    array('%feature_name' => $feature->name, '%name' => $info->name)),
    'admin/ecsettings/ptypes', t('This action cannot be undone'), t('Delete feature'),
    t('Cancel'));
}

/**
 * Confirm if the product feature should be deleted
 *
 * @param $info
 * @param $features
 *   Object, the feature
 * @ingroup form
 */
function ec_product_admin_ptypes_feature_delete_submit(&$form, &$form_state) {
  ec_product_feature_disable($form_state['values']['info'], $form_state['values']['feature']->ftype);
  $form_state['redirect'] = 'admin/ecsettings/ptypes/'. $form_state['values']['info']->ptype .'/features';
}

/**
 * Display all products in a table format.
 * TODO: can this be done with views? Probably need appropriate 'Operations' field...
 */
function ec_product_overview() {
  $header = array(
    array('data' => t('ID'), 'field' => 'p.nid', 'sort' => 'asc'),
    array('data' => t('SKU'), 'field' => 'p.sku'),
    array('data' => t('Title'), 'field' => 'n.title'),
    array('data' => t('Price'), 'field' => 'p.price'),
    array('data' => t('Type'), 'field' => 'p.ptype'),
    array('data' => t('Operations'))
  );
  $sql = 'SELECT n.nid, n.title, p.* FROM {node} n
    INNER JOIN {ec_product} p ON n.vid = p.vid
    WHERE n.status = 1'. tablesort_sql($header);
  $result = pager_query(db_rewrite_sql($sql), 50);
  while ($data = db_fetch_object($result)) {
    $rows[] = array(
      $data->nid,
      empty($data->sku) ? '' : $data->sku,
      l($data->title, "node/$data->nid"),
      format_currency(ec_product_get_final_price($data, 'ec_product')),
      $data->ptype,
      l(t('Edit'), "node/$data->nid/edit")
    );
  }
  $pager = theme('pager', NULL, 50, 0);
  if (!empty($pager)) {
    $rows[] = array(array('data' => $pager, 'colspan' => 6));
  }
  return theme('ec_product_overview', $rows, $header);
}

/**
 * Generate a hardcoded product list. Uses echo to
 * directly print the result.
 * TODO: can this be done with views? At least this use the product list
 * above and print raw to the page.
 */
function ec_product_quicklist() {
  $html = ec_store_quicklist_header();
  $header = array(
    array('data' => t('Id'), 'field' => 'p.nid', 'sort' => 'asc'),
    array('data' => t('SKU'), 'field' => 'p.sku'),
    array('data' => t('Title'), 'field' => 'n.title'),
    array('data' => t('Price'), 'field' => 'p.price'),
    array('data' => t('Type'), 'field' => 'p.ptype')
  );
  $sql = 'SELECT * FROM {node} n
    INNER JOIN {ec_product} p ON n.vid = p.vid
    WHERE n.status = 1'. tablesort_sql($header);
  $result = pager_query(db_rewrite_sql($sql), 50);
  while ($data = db_fetch_object($result)) {
    $rows[] = array($data->nid, $data->sku, $data->title,
      format_currency(ec_product_get_final_price($data, 'product')), $data->ptype);
  }
  $pager = theme('pager', NULL, 50, 0);
  if (!empty($pager)) {
    $rows[] = array(array('data' => $pager, 'colspan' => 5));
  }
  echo $html;
  if (!empty($rows)) {
    echo theme('table', $header, $rows);
  }
  else {
    echo '<p>'. t('There are no products to view. You can <a href="!add_product">add a product</a>.',
      array('!add_product' => 'node/add/product')) .'</p>';
  }
  echo '</body></html>';
}
