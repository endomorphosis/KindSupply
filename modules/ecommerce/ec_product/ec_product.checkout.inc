<?php
// $Id: ec_product.checkout.inc,v 1.1.2.8 2009/03/09 01:51:53 gordon Exp $
/**
 * @file
 * Provide checkout functions for product
 */

/**
 * Implementation of hook_checkout_init().
 */
function ec_product_checkout_init(&$form_state) {
  $txn =& $form_state['storage']['txn'];

  $txn->shippable = 0;
  foreach ($txn->items as $nid => $product) {
    if (ec_product_is_shippable($product->vid)) {
      $txn->shippable = 1;
      $txn->items[$nid]->shippable = 1;
    }
  }
}

/**
 * Implementation of hook_checkout_calculate().
 */
function ec_product_checkout_calculate(&$form_state) {
  $txn =& $form_state['storage']['txn'];

  // process any specials and add them to misc
  foreach ($txn->items as $item) {
    foreach (ec_product_specials_get($item, 'checkout', FALSE, $txn) as $type => $special) {
      if (!is_array($special)) {
        $special = array('price' => $special);
      }
      $misc = new stdClass;
      $misc->type = 'special_'. (isset($special['type']) ? $special['type'] : $type);
      $misc->vid = $item->vid;
      $misc->description = isset($special['description']) ? $special['description'] : 'special';
      $misc->invisible = isset($special['invisible']) ? $special['invisible'] : 1;
      $misc->price = $special['price'];
      $misc->qty = ec_product_has_quantity($item) ? ($special['qty'] ? $special['qty'] : $item->qty) : 1;
      $txn->misc[] = $misc;
    }
  }
}

/**
 * Implementation of hook_checkout_post_checkout().
 */
function ec_product_checkout_post_submit(&$txn) {
  $ret = NULL;
  if (!empty($txn->items)) {
    foreach ($txn->items as $item) {
      $goto = ec_product_invoke_productapi($item, 'checkout_post_submit', $txn);
      if ($goto) {
        $ret = $goto;
      }
      if ($features = ec_product_ptypes_get('features', $item)) {
        foreach ($features as $feature) {
          $goto = ec_product_invoke_feature($feature->ftype, $item, 'checkout_post_submit', $txn, $ret);
          if ($goto) {
            $ret = $goto;
          }
        }
      }
    }
  }

  if ($ret) {
    return $ret;
  }
}

