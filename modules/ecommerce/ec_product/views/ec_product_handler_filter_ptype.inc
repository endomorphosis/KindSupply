<?php
// $Id: ec_product_handler_filter_ptype.inc,v 1.1.2.1 2009/02/25 22:52:23 darrenoh Exp $

class ec_product_handler_filter_ptype extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Product type');
      $ptypes = ec_product_ptypes_get('names');
      foreach ($ptypes as $ptype => $name) {
        $options[$ptype] = $name;
      }
      $this->value_options = $options;
    }
  }
}
