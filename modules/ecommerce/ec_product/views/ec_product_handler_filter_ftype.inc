<?php
// $Id: ec_product_handler_filter_ftype.inc,v 1.1.2.1 2009/02/25 22:52:23 darrenoh Exp $

class ec_product_handler_filter_ftype extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Product feature');
      $ftypes = ec_product_feature_get('names');
      foreach ($ftypes as $ftype => $name) {
        $options[$ftype] = $name;
      }
      $this->value_options = $options;
    }
  }
}
