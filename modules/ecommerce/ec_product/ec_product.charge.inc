<?php
// $Id: ec_product.charge.inc,v 1.1.2.2 2009/03/18 12:46:52 gordon Exp $

/**
 * Implementation of hook_filter_process().
 */
function ec_product_shippable_filter_process($settings, $txn) {
  return $txn->shippable ? TRUE : FALSE;
}

/**
 * Implementation of hook_variable_process().
 */
function ec_product_shippable_variable_process($settings, $txn) {
  $txn = drupal_clone($txn);
  $value = 0;

  $items = array_filter($txn->items, '_ec_product_shippable_filter');

  if (!empty($items)) {
    foreach ($items as $item) {
      $value+= ec_store_adjust_misc($txn, $item);
    }
  }

  return $value;
}

function _ec_product_shippable_filter($a) {
  return $a->shippable;
}
