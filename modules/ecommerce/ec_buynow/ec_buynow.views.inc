<?php
// $Id: ec_buynow.views.inc,v 1.1.2.1 2009/02/28 12:19:52 gordon Exp $
/**
 * @file
 * Implement Views integration into BuyNow
 */

/**
 * Implementationm of hook_views_data().
 */
function ec_buynow_views_data() {
  $data['ec_product']['hide_buynow_link'] = array(
    'title' => t('Hide buy now link'),
    'help' => t('Lists products where the buy now link is not used.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Hide Buynow'),
    ),
  );

  return $data;
}

