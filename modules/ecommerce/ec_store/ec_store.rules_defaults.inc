<?php
// $Id: ec_store.rules_defaults.inc,v 1.1.2.4 2009/02/09 18:57:07 recidive Exp $

/**
 * @file
 * Implement default rules.
 */

/**
 * Implementation of hook_rules_defaults().
 */
function ec_store_rules_defaults() {
  $config = array (
  'rules' => 
  array (
    'rules_7' => 
    array (
      '#type' => 'rule',
      '#set' => 'event_ec_store_event_transactions_bef_save',
      '#label' => 'Shippable orders now in picking',
      '#active' => 1,
      '#weight' => '0',
      '#status' => 'custom',
      '#conditions' => 
      array (
        0 => 
        array (
          '#weight' => 0,
          '#info' => 
          array (
            'label' => 'Transaction is shippable',
            'arguments' => 
            array (
              'txn' => 
              array (
                'type' => 'transaction',
                'label' => 'Transaction',
              ),
            ),
            'module' => 'Transaction',
          ),
          '#name' => 'ec_store_condition_is_shippable',
          '#settings' => 
          array (
            '#argument map' => 
            array (
              'txn' => 'txn',
            ),
          ),
          '#type' => 'condition',
        ),
        1 => 
        array (
          '#type' => 'condition',
          '#settings' => 
          array (
            '#argument map' => 
            array (
              'txn' => 'txn',
              'orig_txn' => 'orig_txn',
            ),
          ),
          '#name' => 'ec_store_condition_allocation_status_changed',
          '#info' => 
          array (
            'label' => 'Transaction allocation status changed',
            'arguments' => 
            array (
              'txn' => 
              array (
                'type' => 'transaction',
                'label' => 'Transaction',
              ),
              'orig_txn' => 
              array (
                'type' => 'transaction',
                'label' => 'Transaction',
              ),
            ),
            'module' => 'Transaction',
          ),
          '#weight' => 0,
        ),
        2 => 
        array (
          '#weight' => 0,
          '#info' => 
          array (
            'label' => 'Transaction allocation status',
            'arguments' => 
            array (
              'txn' => 
              array (
                'type' => 'transaction',
                'label' => 'Transaction',
              ),
            ),
            'module' => 'Transaction',
          ),
          '#name' => 'ec_store_condition_allocation_status',
          '#settings' => 
          array (
            'allocation' => 
            array (
              2 => '2',
            ),
            '#argument map' => 
            array (
              'txn' => 'txn',
            ),
          ),
          '#type' => 'condition',
        ),
        3 => 
        array (
          '#type' => 'condition',
          '#settings' => 
          array (
            'workflow' => 
            array (
              1 => '1',
            ),
            '#argument map' => 
            array (
              'txn' => 'txn',
            ),
          ),
          '#name' => 'ec_store_condition_workflow_status',
          '#info' => 
          array (
            'label' => 'Transaction workflow status',
            'arguments' => 
            array (
              'txn' => 
              array (
                'type' => 'transaction',
                'label' => 'Transaction',
              ),
            ),
            'module' => 'Transaction',
          ),
          '#weight' => 0,
        ),
      ),
      '#actions' => 
      array (
        0 => 
        array (
          '#weight' => 0,
          '#info' => 
          array (
            'label' => 'Set transaction workflow',
            'arguments' => 
            array (
              'transaction' => 
              array (
                'type' => 'transaction',
                'label' => 'Transaction',
              ),
            ),
            'module' => 'Transaction',
          ),
          '#name' => 'ec_store_rules_action_set_workflow',
          '#settings' => 
          array (
            'workflow' => '10',
            '#argument map' => 
            array (
              'txn' => 'transaction',
            ),
          ),
          '#type' => 'action',
        ),
      ),
    ),
    'rules_8' => 
    array (
      '#type' => 'rule',
      '#set' => 'event_ec_store_event_transactions_save',
      '#label' => 'Mark shipped product as completed',
      '#active' => 1,
      '#weight' => '0',
      '#status' => 'custom',
      '#conditions' => 
      array (
        0 => 
        array (
          '#weight' => 0,
          '#info' => 
          array (
            'label' => 'Transaction workflow status changed',
            'arguments' => 
            array (
              'txn' => 
              array (
                'type' => 'transaction',
                'label' => 'Transaction',
              ),
              'orig_txn' => 
              array (
                'type' => 'transaction',
                'label' => 'Transaction',
              ),
            ),
            'module' => 'Transaction',
          ),
          '#name' => 'ec_store_condition_workflow_status_changed',
          '#type' => 'condition',
        ),
        1 => 
        array (
          '#type' => 'condition',
          '#settings' => 
          array (
            'workflow' => 
            array (
              3 => '3',
            ),
          ),
          '#name' => 'ec_store_condition_workflow_status',
          '#info' => 
          array (
            'label' => 'Transaction workflow status',
            'arguments' => 
            array (
              'txn' => 
              array (
                'type' => 'transaction',
                'label' => 'Transaction',
              ),
            ),
            'module' => 'Transaction',
          ),
          '#weight' => 0,
        ),
      ),
      '#actions' => 
      array (
        0 => 
        array (
          '#weight' => 0,
          '#info' => 
          array (
            'arguments' => 
            array (
              'task_date' => 
              array (
                'type' => 'date',
                'label' => 'Scheduled evaluation date',
              ),
              'transaction' => 
              array (
                'label' => 'Transaction',
                'type' => 'transaction',
              ),
            ),
            'label' => 'Schedule Schedule flaging transaction as completed',
            'status' => 'custom',
            'module' => 'Rule Scheduler',
            'base' => 'rules_scheduler_action',
            'set' => 'rules_txn_complete',
          ),
          '#name' => 'rules_action_schedule_set_rules_txn_complete',
          '#settings' => 
          array (
            'task_date' => '+12 hours',
          ),
          '#type' => 'action',
        ),
      ),
    ),
    'rules_9' => 
    array (
      '#type' => 'rule',
      '#set' => 'rules_txn_complete',
      '#label' => 'Transaction Complete',
      '#active' => 1,
      '#weight' => '0',
      '#status' => 'custom',
      '#conditions' => 
      array (
        0 => 
        array (
          '#type' => 'condition',
          '#settings' => 
          array (
            'workflow' => 
            array (
              3 => '3',
            ),
          ),
          '#name' => 'ec_store_condition_workflow_status',
          '#info' => 
          array (
            'label' => 'Transaction workflow status',
            'arguments' => 
            array (
              'txn' => 
              array (
                'type' => 'transaction',
                'label' => 'Transaction',
              ),
            ),
            'module' => 'Transaction',
          ),
          '#weight' => 0,
        ),
      ),
      '#actions' => 
      array (
        0 => 
        array (
          '#weight' => 0,
          '#info' => 
          array (
            'label' => 'Set transaction workflow',
            'arguments' => 
            array (
              'transaction' => 
              array (
                'type' => 'transaction',
                'label' => 'Transaction',
              ),
            ),
            'module' => 'Transaction',
          ),
          '#name' => 'ec_store_rules_action_set_workflow',
          '#settings' => 
          array (
            'workflow' => '6',
          ),
          '#type' => 'action',
        ),
      ),
    ),
    'rules_6' => 
    array (
      '#type' => 'rule',
      '#set' => 'event_ec_store_event_transactions_bef_save',
      '#label' => 'Set non-shippable transaction as complete',
      '#active' => 1,
      '#weight' => '0',
      '#status' => 'custom',
      '#conditions' => 
      array (
        3 => 
        array (
          '#settings' => 
          array (
            '#argument map' => 
            array (
              'txn' => 'txn',
            ),
          ),
          '#type' => 'condition',
          '#name' => 'ec_store_condition_is_shippable',
          '#info' => 
          array (
            'label' => 'Transaction is shippable',
            'arguments' => 
            array (
              'txn' => 
              array (
                'type' => 'transaction',
                'label' => 'Transaction',
              ),
            ),
            'module' => 'Transaction',
          ),
          '#weight' => -1,
          '#negate' => 1,
        ),
        0 => 
        array (
          '#type' => 'condition',
          '#settings' => 
          array (
            '#argument map' => 
            array (
              'txn' => 'txn',
              'orig_txn' => 'orig_txn',
            ),
          ),
          '#name' => 'ec_store_condition_allocation_status_changed',
          '#info' => 
          array (
            'label' => 'Transaction allocation status changed',
            'arguments' => 
            array (
              'txn' => 
              array (
                'type' => 'transaction',
                'label' => 'Transaction',
              ),
              'orig_txn' => 
              array (
                'type' => 'transaction',
                'label' => 'Transaction',
              ),
            ),
            'module' => 'Transaction',
          ),
          '#weight' => 0,
        ),
        1 => 
        array (
          '#weight' => 0,
          '#info' => 
          array (
            'label' => 'Transaction allocation status',
            'arguments' => 
            array (
              'txn' => 
              array (
                'type' => 'transaction',
                'label' => 'Transaction',
              ),
            ),
            'module' => 'Transaction',
          ),
          '#name' => 'ec_store_condition_allocation_status',
          '#settings' => 
          array (
            'allocation' => 
            array (
              2 => '2',
            ),
            '#argument map' => 
            array (
              'txn' => 'txn',
            ),
          ),
          '#type' => 'condition',
        ),
        2 => 
        array (
          '#type' => 'condition',
          '#settings' => 
          array (
            'workflow' => 
            array (
              1 => '1',
              2 => '2',
            ),
            '#argument map' => 
            array (
              'txn' => 'txn',
            ),
          ),
          '#name' => 'ec_store_condition_workflow_status',
          '#info' => 
          array (
            'label' => 'Transaction workflow status',
            'arguments' => 
            array (
              'txn' => 
              array (
                'type' => 'transaction',
                'label' => 'Transaction',
              ),
            ),
            'module' => 'Transaction',
          ),
          '#weight' => 0,
        ),
      ),
      '#actions' => 
      array (
        1 => 
        array (
          '#type' => 'action',
          '#settings' => 
          array (
            'workflow' => '6',
            '#argument map' => 
            array (
              'txn' => 'transaction',
            ),
          ),
          '#name' => 'ec_store_rules_action_set_workflow',
          '#info' => 
          array (
            'label' => 'Set transaction workflow',
            'arguments' => 
            array (
              'transaction' => 
              array (
                'type' => 'transaction',
                'label' => 'Transaction',
              ),
            ),
            'module' => 'Transaction',
          ),
          '#weight' => 0,
        ),
      ),
    ),
  ),
  'rule_sets' => 
  array (
    'rules_txn_complete' => 
    array (
      'arguments' => 
      array (
        'transaction' => 
        array (
          'label' => 'Transaction',
          'type' => 'transaction',
        ),
      ),
      'label' => 'Schedule flaging transaction as completed',
      'status' => 'custom',
    ),
  ),
);

  return $config;
}
