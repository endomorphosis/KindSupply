<?php
// $Id: ec_store.inc,v 1.5.2.11 2009/03/16 15:28:23 darrenoh Exp $

/**
 * @file
 * Code here is not specific to any process, might be any user, and is not run infrequently.
 */

/**
 * Saves all addresses in transaction using ec_store_transaction_address_save().
 *
 * @param $txn
 *   Object, transaction with address array.
 */
function ec_store_transaction_addresses_save($txn) {
  // If we where not passed a valid transaction and we can't save.
  if (!isset($txn->txnid) || !is_array($txn->address)) {
    return FALSE;
  }
  foreach ($txn->address as $type => $address) {
    $address['txnid'] = $txn->txnid;
    ec_store_transaction_address_save($address, $type);
  }
}

/**
 * Saves an individual address to the passed transaction.
 *
 * @param $address
 *   Object, address object.
 * @param $type
 *   String, address type(corresponds to type field in database).
 */
function ec_store_transaction_address_save($address, $type) {
  $address['type'] = $type;

  drupal_write_record('ec_transaction_address', $address, array('txnid', 'type'));
  if (!db_affected_rows()) {
    drupal_write_record('ec_transaction_address', $address);
  }
}

/**
 * Creates a standard address form.
 * When passed a display field we filter out all other sections and display only
 * the one given. If passed shipping only a shipping address will be shown.
 *
 * @param $txn
 *   Objectm this is a transaction object used for default values.
 * @param $display
 *   String, option field that allows the form to filter sections.
 */
function ec_store_transaction_addresses_form($txn, $display = NULL) {
  $form['address']['#tree'] = TRUE;

  $form['address']['shipping'] = ec_store_address_form($txn->address['shipping']);
  $form['address']['shipping']['#type'] = 'fieldset';
  $form['address']['shipping']['#title'] = t('Shipping address');

  $form['address']['billing'] = ec_store_address_form($txn->address['billing']);
  $form['address']['billing']['#type'] = 'fieldset';
  $form['address']['billing']['#title'] = t('Billing address');

  switch ($display) {
    case 'shipping':
      unset($form['billing']);
      break;

    case 'billing':
      unset($form['shipping']);
      break;
  }
  return $form;
}

function ec_store_address_form($address = NULL) {
  module_load_include('inc', 'ec_store', 'ec_store.localization');

  if (!isset($address)) {
    $address = (object) array('firstname' => NULL, 'lastname' => NULL, 'street1' => NULL, 'street2' => NULL, 'city' => NULL, 'state' => NULL, 'zip' => NULL);
  }

  if (isset($address->aid)) {
    $form['type'] = array(
      '#type'  => 'hidden',
      '#value' => $address->type,
    );
  }

  $form['firstname'] = array(
    '#type'          => 'textfield',
    '#title'         => t('First name'),
    '#default_value' => $address->firstname,
    '#size'          => 32,
    '#maxlength'     => 32,
  );
  $form['lastname'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Last name'),
    '#default_value' => $address->lastname,
    '#size'          => 32,
    '#maxlength'     => 32,
  );
  $form['country'] = array(
    '#type'          => 'select',
    '#title'         => t('Country'),
    '#default_value' => isset($address->country) ? $address->country : variable_get('ec_country', ''),
    '#options'       => _ec_store_location_countries(),
  );
  $form['street1'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Street'),
    '#default_value' => $address->street1,
    '#size'          => 60,
    '#maxlength'     => 64,
  );
  $form['street2'] = array(
    '#type'          => 'textfield',
    '#default_value' => $address->street2,
    '#size'          => 60,
    '#maxlength'     => 64,
  );
  $form['city'] = array(
    '#type'          => 'textfield',
    '#title'         => t('City'),
    '#default_value' => $address->city,
    '#size'          => 32,
    '#maxlength'     => 32,
  );
  $form['state'] = array(
    '#type'          => 'textfield',
    '#title'         => t('State/Province'),
    '#default_value' => $address->state,
    '#size'          => 32,
    '#maxlength'     => 32,
  );
  $form['zip'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Zip/Postal Code'),
    '#default_value' => $address->zip,
    '#size'          => 10,
    '#maxlength'     => 10,
  );
  return $form;
}
