<?php
// $Id: ec_store.rules_forms.inc,v 1.1.2.4 2009/02/09 18:57:07 recidive Exp $

/**
 * @file
 * Implement supporting forms.
 */

/**
 * Condition: Check the transaction workflow status.
 */
function ec_store_condition_workflow_status_form($settings, &$form) {
  $form['settings']['workflow'] = array(
    '#type' => 'select',
    '#title' => t('Workflow status'),
    '#default_value' => $settings['workflow'],
    '#options' => ec_store_transaction_workflow(),
    '#multiple' => TRUE,
    '#description' => t('Select the workflow statuses which the transaction is set to.'),
  );
}

/**
 * Condition: Check the transaction allocation status.
 */
function ec_store_condition_allocation_status_form($settings, &$form) {
  $form['settings']['allocation'] = array(
    '#type' => 'select',
    '#title' => t('Allocation status'),
    '#default_value' => $settings['allocation'],
    '#options' => ec_store_transaction_allocation(),
    '#multiple' => TRUE,
    '#description' => t('Select the allocation statuses which the transaction is set to.'),
  );
}

/**
 * Action: Set transaction workflow.
 */
function ec_store_rules_action_set_workflow_form($settings, &$form) {
  $form['settings']['workflow'] = array(
    '#type' => 'radios',
    '#title' => t('New workflow status'),
    '#default_value' => $settings['workflow'],
    '#options' => ec_store_transaction_workflow(),
    '#description' => t('Select the workflow that will be set by this action.'),
  );
}
