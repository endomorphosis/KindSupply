<?php
// $Id: ec_store_views_handler_field_name.inc,v 1.1.2.1 2009/03/27 06:24:06 gordon Exp $
/**
 * @file
 * Views handler for creating the Customer name
 */

/**
 * Field handler to provide simple renderer to display the customer name as
 * a single field
 */
class ec_store_views_handler_field_name extends views_handler_field {
  /**
   * Override the init function to add the additional fields for the names
   */
  function init(&$view, &$data) {
    parent::init($view, $data);
    $this->additional_fields['firstname'] = 'firstname';
    $this->additional_fields['lastname'] = 'lastname';

    if ($this->options['include_multiline_address']) {
      $this->additional_fields['street1'] = 'street1';
      $this->additional_fields['street2'] = 'street2';
      $this->additional_fields['city'] = 'city';
      $this->additional_fields['state'] = 'state';
      $this->additional_fields['zip'] = 'zip';
      $this->additional_fields['country'] = 'country';
    }
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['include_multiline_address'] = array('default' => FALSE);
    return $options;
  }

  /**
   * Provide link to node option
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['include_multiline_address'] = array(
      '#title' => t('Include multi-line address'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['include_multiline_address'],
    );
  }

  function render($values) {
    $name = check_plain($values->{$this->aliases['firstname']} .' '. $values->{$this->aliases['lastname']});
    if ($this->options['include_multiline_address']) {
      $schema = drupal_get_schema($this->table);
      $address = array('name' => $name);

      foreach (array_intersect_key($this->aliases, $schema['fields']) as $key => $alias) {
        $address[$key] = $values->$alias;
      }
      return theme('formatted_address', $address);
    }
    return $name;
  }
}
