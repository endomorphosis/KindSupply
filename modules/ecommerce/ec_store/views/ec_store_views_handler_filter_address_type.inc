<?php
// $Id: ec_store_views_handler_filter_address_type.inc,v 1.1.2.3 2008/12/11 05:45:48 gordon Exp $
/**
 * @file
 * Implement filter to select by address types
 */
class ec_store_views_handler_filter_address_type extends views_handler_filter_many_to_one {
  function init(&$view, &$options) {
    parent::init($view, $options);
    $this->options['add_table'] = 'ec_transaction_addrress';
  }

  function get_value_options() {
    $this->value_options = array(
      'billing' => t('Billing'),
      'shipping' => t('Shipping'),
    );
  }

  function query() {
    if (empty($this->value)) {
      return;
    }
    $this->ensure_my_table();
    $placeholder = !empty($this->definition['numeric']) ? '%d' : "'%s'";

    $replace = array_fill(0, sizeof($this->value), $placeholder);
    $in = ' ('. implode(", ", $replace) .')';

    // We use array_values() because the checkboxes keep keys and that can cause
    // array addition problems.
    $this->query->add_where($this->options['group'], "$this->table_alias.$this->real_field IN" . $in ." OR $this->table_alias.$this->real_field IS NULL", array_values($this->value));
  }
}
