<?php
// $Id: ec_store_views_handler_field_workflow.inc,v 1.1.2.3 2009/02/08 21:45:50 gordon Exp $
/**
 * @file
 * Provide output of allocation status description
 */

class ec_store_views_handler_field_workflow extends views_handler_field {
  function sort_clickable($order) {
    $this->query->add_orderby('ec_workflow_statuses', 'weight', $order);
  }

  function render($values) {
    return drupal_ucfirst($values->{$this->field_alias});
  }
}

