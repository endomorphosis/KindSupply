<?php
// $Id: ec_store_views_handler_field_allocation.inc,v 1.1.2.1 2008/11/20 20:02:58 brmassa Exp $
/**
 * @file
 * Provide output of allocation status description
 */

class ec_store_views_handler_field_allocation extends views_handler_field {
  function render($values) {
    return ec_store_transaction_get_allocation($values->{$this->field_alias});
  }
}

