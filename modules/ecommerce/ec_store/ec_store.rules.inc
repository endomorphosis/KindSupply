<?php
// $Id: ec_store.rules.inc,v 1.1.2.5 2009/02/25 21:31:22 darrenoh Exp $

/**
 * @file
 * Implementation of rules functionality
 */

/**
 * Implementation of hook_rules_event_info().
 */
function ec_store_rules_event_info() {
  return array(
    'ec_store_event_transactions_bef_save' => array(
      'label' => t('Before transaction saved'),
      'module' => 'Transaction',
      'arguments' => array(
        'txn' => array('type' => 'transaction', 'label' => t('Updated transaction')),
        'orig_txn' => array('type' => 'transaction', 'label' => t('Unchanged transaction')),
      ),
    ),
    'ec_store_event_transactions_save' => array(
      'label' => t('After transaction saved'),
      'module' => 'Transaction',
      'arguments' => array(
        'txn' => array('type' => 'transaction', 'label' => t('Updated transaction')),
        'orig_txn' => array('type' => 'transaction', 'label' => t('Unchanged transaction')),
      ),
    ),
  );
}

/**
 * Implementation of hook_rules_condition_info().
 */
function ec_store_rules_condition_info() {
  return array(
    'ec_store_condition_workflow_status' => array(
      'label' => t('Transaction workflow status'),
      'arguments' => array(
        'txn' => array('type' => 'transaction', 'label' => t('Transaction')),
      ),
      'module' => 'Transaction',
    ),
    'ec_store_condition_workflow_status_changed' => array(
      'label' => t('Transaction workflow status changed'),
      'arguments' => array(
        'txn' => array('type' => 'transaction', 'label' => t('Transaction')),
        'orig_txn' => array('type' => 'transaction', 'label' => t('Transaction')),
      ),
      'module' => 'Transaction',
    ),
    'ec_store_condition_allocation_status' => array(
      'label' => t('Transaction allocation status'),
      'arguments' => array(
        'txn' => array('type' => 'transaction', 'label' => t('Transaction')),
      ),
      'module' => 'Transaction',
    ),
    'ec_store_condition_allocation_status_changed' => array(
      'label' => t('Transaction allocation status changed'),
      'arguments' => array(
        'txn' => array('type' => 'transaction', 'label' => t('Transaction')),
        'orig_txn' => array('type' => 'transaction', 'label' => t('Transaction')),
      ),
      'module' => 'Transaction',
    ),
    'ec_store_condition_is_shippable' => array(
      'label' => t('Transaction is shippable'),
      'arguments' => array(
        'txn' => array('type' => 'transaction', 'label' => t('Transaction')),
      ),
      'module' => 'Transaction',
    ),
  );
}

/**
 * Condition: Check transaction status.
 */
function ec_store_condition_workflow_status(&$txn, $settings) {
  return in_array($txn->workflow, $settings['workflow']);
}

/**
 * Condition: Transaction workflow status has changed.
 */
function ec_store_condition_workflow_status_changed(&$txn, &$orig_txn, $settings) {
  return (!$orig_txn || $txn->workflow != $orig_txn->workflow);
}

/**
 * Condition: Check transaction status.
 */
function ec_store_condition_allocation_status(&$txn, $settings) {
  return isset($txn->allocation) && in_array($txn->allocation, $settings['allocation']);
}

/**
 * Condition: Transaction allocation status has changed.
 */
function ec_store_condition_allocation_status_changed(&$txn, &$orig_txn, $settings) {
  return (!$orig_txn || $txn->allocation != $orig_txn->allocation);
}

/**
 * Condition: Transaction is shippable.
 */
function ec_store_condition_is_shippable(&$txn, $settings) {
  return isset($txn->shippable) && $txn->shippable ? TRUE : FALSE;
}

/**
 * Implementation of hook_rules_action_info().
 */
function ec_store_rules_action_info() {
  return array(
    'ec_store_rules_action_set_workflow' => array(
      'label' => t('Set transaction workflow'),
      'arguments' => array(
        'transaction' => array('type' => 'transaction', 'label' => t('Transaction')),
      ),
      'module' => 'Transaction',
    ),
  );
}

/**
 * Action: Set transaction workflow .
 */
function ec_store_rules_action_set_workflow(&$txn, $settings) {
  $txn->workflow = $settings['workflow'];
  return array('transaction' => $txn);
}

/**
 * Implementation of hook_rules_action_info_alter().
 */
function ec_store_rules_action_info_alter(&$actions) {
  unset($actions['rules_core_ec_store_action_set_workflow']);
}

/**
 * Implementation of hook_rules_data_type_info().
 */
function ec_store_rules_data_type_info() {
  return array(
    'transaction' => array(
      'label' => t('Transaction'),
      'class' => 'ec_store_rules_data_type_transaction',
      'savable' => TRUE,
      'module' => 'Transaction',
    ),
  );
}

class ec_store_rules_data_type_transaction extends rules_data_type {
  function save() {
    $txn =& $this->get();
    ec_store_transaction_save($txn);
    return TRUE;
  }

  function load($txnid) {
    return ec_store_transaction_load($txnid);
  }

  function get_identifier() {
    $txn =& $this->get();
    return $txn->txnid;
  }
}
